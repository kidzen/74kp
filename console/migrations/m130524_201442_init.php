<?php

use yii\db\Migration;
    // public function beforeSave($insert)
    // {
    //     if (parent::beforeSave($insert)) {
    //     // Place your custom code here
    //         $post = \Yii::$app->request->post();
    //         if($post['VehicleAssignment']){
    //             $baseVehicle = Vehicle::findOne($post['VehicleAssignment']['vehicle_id']);
    //             $baseVehicle->task_id = $post['VehicleAssignment']['c_task_id'];
    //             $baseVehicle->holder_id = $post['VehicleAssignment']['c_holder_id'];
    //             $baseVehicle->rank_id = $post['VehicleAssignment']['c_rank_id'];
    //             $baseVehicle->status_id = $post['VehicleAssignment']['c_status_id'];
    //             $baseVehicle->odometer = $post['VehicleAssignment']['c_odometer'];
    //             $baseVehicle->progress = $post['VehicleAssignment']['c_progress'];
    //             $baseVehicle->save();
    //         }
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }

class m130524_201442_init extends Migration
{
	public function up()
	{

		if(Yii::$app->db->driverName == 'mysql'){
			try {
				$sc = Yii::$app->db->createCommand("SET FOREIGN_KEY_CHECKS = 0")->execute();
				$tables = Yii::$app->db->createCommand("
					select concat('DROP TABLE IF EXISTS ', table_name) as 'sql'
					FROM information_schema.tables
					WHERE table_schema = '74kp' and table_name != 'migration'
					")->queryAll();
				foreach ($tables as $table) {
					var_dump($table['sql']);
					$dropTable = Yii::$app->db->createCommand($table['sql']);
					$dropTable->execute();
				}

                // $views = Yii::$app->db->createCommand("select 'drop view \"' || view_name || '\" cascade constraints' as sql from user_views ")->queryAll();
                // foreach ($views as $view) {
                //  var_dump($view['SQL']);
                //  $dropView = Yii::$app->db->createCommand($view['SQL']);
                //  $dropView->execute();
                // }
			} catch (\yii\db\Exception $e) {
				echo "error";
			}
		}
		$sc = Yii::$app->db->createCommand("SET FOREIGN_KEY_CHECKS = 1")->execute();

        // die();

		$tableOptions = null;
		if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}


		$this->createTable('{{%role}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string()->notNull()->unique(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%user}}', [
			'id' => $this->primaryKey(),
			'username' => $this->string()->notNull()->unique(),
			'role_id' => $this->integer()->notNull(),
			'auth_key' => $this->string(32)->notNull(),
			'password_hash' => $this->string()->notNull(),
			'password_reset_token' => $this->string()->unique(),
			'email' => $this->string()->notNull()->unique(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%staff_profile}}', [
			'id' => $this->primaryKey(),
			'user_id' => $this->integer(),
			'name' => $this->string()->notNull(),
			'nickname' => $this->string(20)->notNull(),
			'staff_no' => $this->string(20)->notNull(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%department}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string()->notNull(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%position}}', [
			'id' => $this->primaryKey(),
			'department_id' => $this->integer(),
			'name' => $this->string()->notNull(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%hod}}', [
			'id' => $this->primaryKey(),
			'staff_id' => $this->integer()->notNull(),
			'department_id' => $this->integer()->notNull(),
			'validity_start' => $this->date(),
			'validity_end' => $this->date(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%account}}', [
			'id' => $this->primaryKey(),
			'staff_id' => $this->integer(),
			'bank_branch' => $this->string(),
			'bank_name' => $this->string()->notNull(),
			'account_no' => $this->string()->notNull(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%leave_type}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%user_leave_setting}}', [
			'id' => $this->primaryKey(),
			'staff_id' => $this->integer(),
			'additional' => $this->float(),
			'expired' => $this->date(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%leave_setting}}', [
			'id' => $this->primaryKey(),
			'position_id' => $this->integer(),
			'grade_id' => $this->integer(),
			'appointed_leave' => $this->float(),
			'additional' => $this->float(),
			'expired' => $this->date(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%leave_request}}', [
			'id' => $this->primaryKey(),
			'staff_id' => $this->integer(),
			'leave_type' => $this->integer(),
			'date_from' => $this->datetime(),
			'date_to' => $this->datetime(),
			'request_at' => $this->datetime(),
			'approved' => $this->double(),
			'approved_at' => $this->datetime(),
			'approved_by' => $this->integer(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%employment_type}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%employment}}', [
			'id' => $this->primaryKey(),
			'staff_id' => $this->integer(),
			'employment_type' => $this->integer(),
			'position_id' => $this->integer(),
			'date_from' => $this->datetime(),
			'date_to' => $this->datetime(),
			'request_at' => $this->datetime(),
			'approved' => $this->double(),
			'approved_at' => $this->datetime(),
			'approved_by' => $this->integer(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%payroll_payment_type}}', [
			'id' => $this->primaryKey(),
			'payroll_id' => $this->integer(),
			'payment_type' => $this->integer(),
			'valid_from' => $this->datetime(),
			'valid_to' => $this->datetime(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%payroll}}', [
			'id' => $this->primaryKey(),
			'employment_id' => $this->integer(),
			'payment_type' => $this->integer(),
			'valid_from' => $this->datetime(),
			'valid_to' => $this->datetime(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%payroll_base}}', [
			'id' => $this->primaryKey(),
			'employment_id' => $this->integer(),
			'valid_from' => $this->datetime(),
			'valid_to' => $this->datetime(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%payroll_deduction}}', [
			'id' => $this->primaryKey(),
			'employment_id' => $this->integer(),
			'valid_from' => $this->datetime(),
			'valid_to' => $this->datetime(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);

		$this->createTable('{{%vehicle_breakdown_type}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%vehicle_task_type}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%vehicle_holder_type}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%vehicle_rank_type}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%vehicle_asset_type}}', [
			'id' => $this->primaryKey(),
			'name' => $this->string(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%vehicle_type}}', [
			'id' => $this->primaryKey(),
			'asset_type' => $this->integer(),
			'name' => $this->string(),
			'description' => $this->string(),
			'engine' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%vehicle_status_type}}', [
			'id' => $this->primaryKey(),
			'code_status' => $this->string(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%vehicle_status}}', [
			'id' => $this->primaryKey(),
			'code_status' => $this->string(),
			'description' => $this->string(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%vehicle}}', [
			'id' => $this->primaryKey(),
			'type' => $this->integer(),
			'model' => $this->string(),
			'plate_no' => $this->string()->notNull()->unique(),
			'task_id' => $this->integer(),
			'holder_id' => $this->integer(),
			'rank_id' => $this->integer(),
			'status_id' => $this->integer(),
			'breakdown_id' => $this->integer(),
			'odometer' => $this->float(),
			'progress' => $this->integer(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);
		$this->createTable('{{%vehicle_assignment}}', [
			'id' => $this->primaryKey(),
			'assignment_no' => $this->string(),
			'vehicle_id' => $this->integer(),
			'c_driver_id' => $this->integer(),
			'c_task_id' => $this->integer(),
			'c_holder_id' => $this->integer(),
			'c_rank_id' => $this->integer(),
			'c_status_id' => $this->integer(),
			// 'c_breakdown_id' => $this->integer(),
			'c_odometer' => $this->float(),
			'c_progress' => $this->integer(),
			'request_at' => $this->date(),
			'request_by' => $this->integer(),
			'required_at' => $this->date(),
			'checked_at' => $this->date(),
			'checked_by' => $this->integer(),
			'approved' => $this->integer(),
			'approved_by' => $this->integer(),
			'approved_at' => $this->date(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);

		$this->createTable('{{%vehicle_check_instruction}}', [
			'id' => $this->primaryKey(),
			'instruction_no' => $this->string(),
			'description' => $this->date(),
			'department_id' => $this->integer(),
			'vehicle_id' => $this->integer(),
			'c_driver_id' => $this->integer(),
			'c_task_id' => $this->integer(),
			'c_holder_id' => $this->integer(),
			'c_rank_id' => $this->integer(),
			'c_status_id' => $this->integer(),
			'c_breakdown_id' => $this->integer(),
			'c_odometer' => $this->float(),
			'c_progress' => $this->integer(),
			'request_at' => $this->date(),
			'request_by' => $this->integer(),
			'required_at' => $this->date(),
			'checked_at' => $this->date(),
			'checked_by' => $this->integer(),
			'approved' => $this->integer(),
			'approved_by' => $this->integer(),
			'approved_at' => $this->date(),
			'total_cost' => $this->float(),

			'status' => $this->smallInteger()->defaultValue(1),
			'deleted' => $this->smallInteger()->defaultValue(0),
			'deleted_at' => $this->timestamp(),
			'created_at' => $this->timestamp(),
			'updated_at' => $this->timestamp(),
			'created_by' => $this->integer(),
			'updated_by' => $this->integer(),
			], $tableOptions);

		$this->insertData();
// leave & payrol
		$this->addForeignKey('fk1','{{%user}}','role_id','{{%role}}','id');
		$this->addForeignKey('fk2','{{%employment}}','position_id','{{%position}}','id');
		$this->addForeignKey('fk3','{{%hod}}','department_id','{{%department}}','id');
		$this->addForeignKey('fk4','{{%hod}}','staff_id','{{%staff_profile}}','id');
		$this->addForeignKey('fk5','{{%account}}','staff_id','{{%staff_profile}}','id');
		$this->addForeignKey('fk6','{{%user_leave_setting}}','staff_id','{{%staff_profile}}','id');
		$this->addForeignKey('fk7','{{%leave_setting}}','position_id','{{%position}}','id');
		$this->addForeignKey('fk8','{{%leave_request}}','leave_type','{{%leave_type}}','id');
		$this->addForeignKey('fk9','{{%staff_profile}}','user_id','{{%user}}','id');
		$this->addForeignKey('fk10','{{%position}}','department_id','{{%department}}','id');
		$this->addForeignKey('fk11','{{%leave_request}}','staff_id','{{%staff_profile}}','id');
		$this->addForeignKey('fk12','{{%leave_request}}','approved_by','{{%staff_profile}}','id');
		$this->addForeignKey('fk13','{{%employment}}','staff_id','{{%staff_profile}}','id');
		$this->addForeignKey('fk14','{{%employment}}','employment_type','{{%employment_type}}','id');
		$this->addForeignKey('fk15','{{%employment}}','position_id','{{%position}}','id');
		$this->addForeignKey('fk16','{{%employment}}','approved_by','{{%staff_profile}}','id');
// sisken
		$this->addForeignKey('fk17','{{%vehicle}}','type','{{%vehicle_type}}','id');
		$this->addForeignKey('fk18','{{%vehicle}}','task_id','{{%vehicle_task_type}}','id');
		$this->addForeignKey('fk19','{{%vehicle}}','holder_id','{{%vehicle_holder_type}}','id');
		$this->addForeignKey('fk20','{{%vehicle}}','rank_id','{{%vehicle_rank_type}}','id');
		$this->addForeignKey('fk21','{{%vehicle}}','status_id','{{%vehicle_status_type}}','id');
		$this->addForeignKey('fk22','{{%vehicle}}','breakdown_id','{{%vehicle_breakdown_type}}','id');

		$this->addForeignKey('fk23','{{%vehicle_assignment}}','vehicle_id','{{%vehicle}}','id');
		$this->addForeignKey('fk24','{{%vehicle_assignment}}','c_driver_id','{{%user}}','id');
		$this->addForeignKey('fk25','{{%vehicle_assignment}}','c_task_id','{{%vehicle_task_type}}','id');
		$this->addForeignKey('fk26','{{%vehicle_assignment}}','c_holder_id','{{%vehicle_holder_type}}','id');
		$this->addForeignKey('fk27','{{%vehicle_assignment}}','c_rank_id','{{%vehicle_rank_type}}','id');
		$this->addForeignKey('fk28','{{%vehicle_assignment}}','c_status_id','{{%vehicle_status_type}}','id');
		// $this->addForeignKey('fk29','{{%vehicle_assignment}}','c_breakdown_id','{{%vehicle_breakdown_type}}','id');

		$this->addForeignKey('fk30','{{%vehicle_check_instruction}}','vehicle_id','{{%vehicle}}','id');
		$this->addForeignKey('fk31','{{%vehicle_check_instruction}}','department_id','{{%department}}','id');
		$this->addForeignKey('fk32','{{%vehicle_check_instruction}}','c_driver_id','{{%user}}','id');
		$this->addForeignKey('fk33','{{%vehicle_check_instruction}}','c_task_id','{{%vehicle_task_type}}','id');
		$this->addForeignKey('fk34','{{%vehicle_check_instruction}}','c_holder_id','{{%vehicle_holder_type}}','id');
		$this->addForeignKey('fk35','{{%vehicle_check_instruction}}','c_rank_id','{{%vehicle_rank_type}}','id');
		$this->addForeignKey('fk36','{{%vehicle_check_instruction}}','c_status_id','{{%vehicle_status_type}}','id');
		$this->addForeignKey('fk37','{{%vehicle_check_instruction}}','c_breakdown_id','{{%vehicle_breakdown_type}}','id');
		$this->addForeignKey('fk38','{{%vehicle_check_instruction}}','request_by','{{%user}}','id');
		$this->addForeignKey('fk39','{{%vehicle_check_instruction}}','checked_by','{{%user}}','id');
		$this->addForeignKey('fk40','{{%vehicle_check_instruction}}','approved_by','{{%user}}','id');

		$this->addForeignKey('fk41','{{%vehicle_type}}','asset_type','{{%vehicle_asset_type}}','id');
		$this->addForeignKey('fk42','{{%vehicle_assignment}}','request_by','{{%user}}','id');
		$this->addForeignKey('fk43','{{%vehicle_assignment}}','approved_by','{{%user}}','id');


	}

	public function down()
	{

		if(Yii::$app->db->driverName == 'mysql'){
			try {
				$tables = Yii::$app->db->createCommand("
					select concat('DROP TABLE IF EXISTS ', table_name) as 'sql'
					FROM information_schema.tables
					WHERE table_schema = '74kp'
					")->queryAll();
				foreach ($tables as $table) {
					var_dump($table['sql']);
					$dropTable = Yii::$app->db->createCommand($table['sql']);
					$dropTable->execute();
				}

                // $views = Yii::$app->db->createCommand("select 'drop view \"' || view_name || '\" cascade constraints' as sql from user_views ")->queryAll();
                // foreach ($views as $view) {
                //  var_dump($view['SQL']);
                //  $dropView = Yii::$app->db->createCommand($view['SQL']);
                //  $dropView->execute();
                // }
			} catch (\yii\db\Exception $e) {
				echo "error";
			}
		}

// drop fk
// 		$this->dropForeignKey('fk1','{{%user}}');
// 		$this->dropForeignKey('fk2','{{%employment}}');
// 		$this->dropForeignKey('fk3','{{%hod}}');
// 		$this->dropForeignKey('fk4','{{%hod}}');
// 		$this->dropForeignKey('fk5','{{%account}}');
// 		$this->dropForeignKey('fk6','{{%user_leave_setting}}');
// 		$this->dropForeignKey('fk7','{{%leave_setting}}');
// 		$this->dropForeignKey('fk8','{{%leave_request}}');
// 		$this->dropForeignKey('fk9','{{%staff_profile}}');
// 		$this->dropForeignKey('fk10','{{%position}}');
// 		$this->dropForeignKey('fk11','{{%leave_request}}');
// 		$this->dropForeignKey('fk12','{{%leave_request}}');
// 		$this->dropForeignKey('fk13','{{%employment}}');
// 		$this->dropForeignKey('fk14','{{%employment}}');
// 		$this->dropForeignKey('fk15','{{%employment}}');
// 		$this->dropForeignKey('fk16','{{%employment}}');
// // drop table
// 		$this->dropTable('{{%role}}');
// 		$this->dropTable('{{%user}}');
// 		$this->dropTable('{{%staff_profile}}');
// 		$this->dropTable('{{%leave_request}}');
// 		$this->dropTable('{{%leave_type}}');
// 		$this->dropTable('{{%user_leave_setting}}');
// 		$this->dropTable('{{%leave_setting}}');
// 		$this->dropTable('{{%account}}');
// 		$this->dropTable('{{%hod}}');
// 		$this->dropTable('{{%position}}');
// 		$this->dropTable('{{%department}}');
// 		$this->dropTable('{{%employment_type}}');
// 		$this->dropTable('{{%employment}}');
	}
	private function insertData()
	{

		$this->insert('{{%role}}', [
			'name' => 'Administrator',
			'description' => 'Admin User',

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%user}}', [
			'username' => 'admin1',
			'role_id' => 1,
			'auth_key' => Yii::$app->security->generateRandomString(),
			'password_hash' => Yii::$app->security->generatePasswordHash('admin1'),
			'email' => 'admin1@mail.com',

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%staff_profile}}', [
			'user_id' => 1,
			'name' => 'SAIFUL ZAHRIN',
			'nickname' => 'Zahrin',
			'staff_no' => '345',

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%department}}', [
			'name' => 'HR',
			'description' => 'Human Resource',

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%position}}', [
			'department_id' => 1,
			'name' => 'Kerani',
			'description' => 'Kerani Am',

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%leave_type}}', [
			'name' => 'Cuti Tahun',
			'description' => $this->string(),

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%leave_setting}}', [
			'position_id' => 1,
			'appointed_leave' => 18,

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%employment_type}}', [
			'name' => 'TETAP',
			'description' => 'Pekerja Tetap',

			'created_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
// vehicle module
		$this->insert('{{%vehicle_holder_type}}', [
			'name' => 'Pegangan KOMP ANG AM',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);

		$this->insert('{{%vehicle_holder_type}}', [
			'name' => 'Pegangan HQ/BEKALAN',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_holder_type}}', [
			'name' => 'Pegangan KOMPOSIT',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_asset_type}}', [
			'name' => 'Kenderaan',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_type}}', [
			'asset_type' => 1,
			'name' => 'HANDALAN 1',
			'engine' => 'MITSUBISHI',

			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_type}}', [
			'asset_type' => 1,
			'name' => 'HANDALAN 2',
			'engine' => 'MITSUBISHI',

			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);

		$this->insert('{{%vehicle_type}}', [
			'asset_type' => 1,
			'name' => 'Motosikal Kriss',
			'engine' => 'Kriss',

			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);

		$this->insert('{{%vehicle_task_type}}', [
			'name' => 'Latihan',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_task_type}}', [
			'name' => 'Peluru',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_task_type}}', [
			'name' => 'Operasi',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_task_type}}', [
			'name' => 'Tadbir',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_breakdown_type}}', [
			'name' => 'Dalam Platun',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_breakdown_type}}', [
			'name' => 'Dalam 21 DBK',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_status_type}}', [
			'code_status' => 'B',
			'description' => 'Kerosakan memerlukan pembaikan di pasukan',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_status_type}}', [
			'code_status' => 'C',
			'description' => 'Kerosakan memerlukan pembaikan di Workshop JLJ',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_status_type}}', [
			'code_status' => 'W',
			'description' => 'Kenderaan/Peralatan dalam Workshop JLJ',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_status_type}}', [
			'code_status' => 'M',
			'description' => 'Membuat rawatan dan senggaraan servis',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_status_type}}', [
			'code_status' => 'X',
			'description' => 'Pemeriksaan Teknikal atau Bulanan BAT M 60/61',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_status_type}}', [
			'code_status' => '(M)',
			'description' => 'Rawatan/Senggaraan telah disiapkan',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);

		$this->insert('{{%vehicle_status_type}}', [
			'code_status' => '(X)',
			'description' => 'Pemeriksaan telah disiapkan',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_status_type}}', [
			'code_status' => 'O',
			'description' => 'Kenderaan/Peralatan boleh digerakan',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_status_type}}', [
			'code_status' => 'TBG',
			'description' => 'Kenderaan belum diselenggara',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle_status_type}}', [
			'code_status' => 'TBW',
			'description' => 'Kenderaan menghampiri waktu selenggara',
			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);
		$this->insert('{{%vehicle}}', [
			'type' => 1,
			'plate_no' => 'ZZ1234',
			'task_id' => 1,
			'holder_id' => 1,
			'status_id' => 8,
			'odometer' => 0,
			'progress' => 1,

			'created_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'updated_at' => new yii\db\Expression('CURRENT_TIMESTAMP'),
			'created_by' => 1,
			'updated_by' => 1,
			]);

	}

}

