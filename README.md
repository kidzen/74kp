Sistem Pengurusan Kenderaan 
===============================

Framework : 
-------------------------
Yii2 Framework

Description
-------------------------
Use to manage and  monitor vehicle assets (registration/maintenance/movement)

Requirement
-------------------------
1 - PHP 5.4^

2 - Database : MySQL

3 - Composer

Yii2 Prerequisite
-------------------------
**this command need to be run once.
```
composer global require "fxp/composer-asset-plugin:^1.3.1"
```

Setup
-------------------------
1 - Clone

2 - Default Database configuration :

```
Host Name : localhost
Database Name : 74kp
Username : root
Password : 
Database Name : 74kp

```
**can be modified from environments/dev or prod/common/config

3 - Open cmd/terminal, and change directory to current this folder directory

4 - On cmd/terminal, run 
```
composer global require "fxp/composer-asset-plugin:^1.3.1"
composer update
init
yii migrate
```

Todo :
-------------------------
1 - Update migration method


DIRECTORY STRUCTURE
-------------------

```
common
    config/              contains shared configurations
    mail/                contains view files for e-mails
    models/              contains model classes used in both backend and frontend
    tests/               contains tests for common classes    
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
backend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains backend configurations
    controllers/         contains Web controller classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for backend application    
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
frontend
    assets/              contains application assets such as JavaScript and CSS
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    tests/               contains tests for frontend application
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
    widgets/             contains frontend widgets
vendor/                  contains dependent 3rd-party packages
environments/            contains environment-based overrides
```
