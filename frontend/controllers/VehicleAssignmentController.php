<?php

namespace frontend\controllers;

use Yii;
// use common\components\Notify;
use common\models\VehicleAssignment;
use common\models\search\VehicleAssignmentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VehicleAssignmentController implements the CRUD actions for VehicleAssignment model.
 */
class VehicleAssignmentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                   'delete' => ['post'],
                ],
            ],
            // 'access-rule' => [
            //     'class' => VerbFilter::className(),
            //     'actions' => [
            //        'delete' => ['post'],
            //     ],
            // ],
        ];
    }

    /**
     * Lists all VehicleAssignment models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new VehicleAssignmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            ]);
    }

    public function actionAssignment()
    {
        // $data = VehicleAssignment::findOne(22);
        // $data = VehicleAssignment::deleteAll();
        // var_dump($data);die();
        $assignmentNo = VehicleAssignment::assignmentNo();
        $searchModel = new VehicleAssignmentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['id'=>'SORT_DESC'];

        $searchModelOnTask = new VehicleAssignmentSearch();
        $dataProviderOnTask = $searchModelOnTask->search(Yii::$app->request->queryParams);
        $dataProviderOnTask->sort->defaultOrder = ['id'=>'SORT_DESC'];
        $dataProviderOnTask->query->andFilterWhere(['or',['c_progress'=>3],['c_progress'=>4]]);
        // var_dump(Yii::$app->request->post());die();
        $model = new VehicleAssignment();
        if ($model->load(Yii::$app->request->post())
            && $model->save()
            ) {
            $mail  = \common\models\User::find()
            ->andFilterWhere(['role_id'=>1])
            // ->andFilterWhere(['not','id'=>Yii::$app->user->id])
            ->andFilterWhere(['status'=>1,'deleted'=>0])
            ->asArray()
            ->all();
            // $model->sendEmail($model,$mail);
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['assignment','#' => 'vehicle-assignment-index']);
        }

        return $this->render('register', [
            'model' => $model,
            'assignmentNo' => $assignmentNo,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModelOnTask' => $searchModelOnTask,
            'dataProviderOnTask' => $dataProviderOnTask,
            ]);
    }

    /**
     * Displays a single VehicleAssignment model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
            ]);
    }

    /**
     * Creates a new VehicleAssignment model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VehicleAssignment();
        $assignmentNo = VehicleAssignment::assignmentNo();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                 'assignmentNo' => $assignmentNo,
           ]);
        }
    }

    /**
     * Updates an existing VehicleAssignment model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $assignmentNo = VehicleAssignment::assignmentNo();
        if (Yii::$app->request->post('_asnew') == '1') {
            $model = new VehicleAssignment();
        }else{
            $model = $this->findModel($id);
            $assignmentNo = $model->assignment_no;
        }

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'assignmentNo' => $assignmentNo,
                ]);
        }
    }
    public function actionApprove($id)
    {
        if(VehicleAssignment::approve($id)){
            Yii::$app->notify->success('Tugasan baru berjaya ditambah.');
        } else {
            Yii::$app->notify->fail();
        }
        return $this->redirect(['/vehicle-assignment/assignment']);
    }
    public function actionReject($id)
    {
        if(VehicleAssignment::reject($id)){
            Yii::$app->notify->success();
        } else {
            Yii::$app->notify->fail();
        }

        return $this->redirect(['/vehicle-assignment/assignment']);
    }
    public function actionStartTask($id)
    {
        if(VehicleAssignment::startTask($id)){
            Yii::$app->notify->success();
        } else {
            Yii::$app->notify->fail();
        }
        return $this->redirect(['/vehicle-assignment/assignment']);
    }
    protected function taskDone($id)
    {
        if(VehicleAssignment::taskDone($id)){
            Yii::$app->notify->success();
        } else {
            Yii::$app->notify->fail();
        }
        return $this->redirect(['/vehicle-assignment/assignment']);
    }

    public function actionRequestTaskDone($id)
    {
        $model = $this->findModel($id);

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->taskDone($model->id);
        } else {
            return $this->render('/vehicle-assignment/request-task-done', [
                'model' => $model,
                ]);
        }

        // return $this->redirect(['/vehicle-assignment/request-task-done', 'id' => $model->id]);
    }

    /**
     * Deletes an existing VehicleAssignment model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['assignment']);
    }

    /**
     *
     * Export VehicleAssignment information into PDF format.
     * @param integer $id
     * @return mixed
     */
    public function actionPdf($id) {
        $model = $this->findModel($id);

        $content = $this->renderAjax('_pdf', [
            'model' => $model,
            ]);

        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE,
            'format' => \kartik\mpdf\Pdf::FORMAT_A4,
            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => \Yii::$app->name],
            'methods' => [
            'SetHeader' => [\Yii::$app->name],
            'SetFooter' => ['{PAGENO}'],
            ]
            ]);

        return $pdf->render();
    }

    /**
    * Creates a new VehicleAssignment model by another data,
    * so user don't need to input all field from scratch.
    * If creation is successful, the browser will be redirected to the 'view' page.
    *
    * @param mixed $id
    * @return mixed
    */
    public function actionSaveAsNew($id) {
        $model = new VehicleAssignment();

        if (Yii::$app->request->post('_asnew') != '1') {
            $model = $this->findModel($id);
        }

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('saveAsNew', [
                'model' => $model,
                ]);
        }
    }

    /**
     * Finds the VehicleAssignment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VehicleAssignment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VehicleAssignment::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
