<?php

namespace frontend\controllers;

use Yii;
use common\models\VehicleCheckInstruction;
use common\models\search\VehicleCheckInstructionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * VehicleCheckInstructionController implements the CRUD actions for VehicleCheckInstruction model.
 */
class VehicleCheckInstructionController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all VehicleCheckInstruction models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VehicleCheckInstructionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('/vehicle-check-instruction/register', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRegister()
    {
        $instructionNo = VehicleCheckInstruction::instructionNo();
        $searchModel = new VehicleCheckInstructionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['id'=>'SORT_DESC'];

        $searchModelOnCheck = new VehicleCheckInstructionSearch();
        $dataProviderOnCheck = $searchModelOnCheck->search(Yii::$app->request->queryParams);
        $dataProviderOnCheck->sort->defaultOrder = ['id'=>'SORT_DESC'];
        $dataProviderOnCheck->query->andFilterWhere(['c_progress'=>2]);
        $model = new VehicleCheckInstruction();
        // var_dump($this->module->requestedRoute);die();
        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {

            return $this->redirect(['/'.$this->module->requestedRoute]);
        }

        return $this->render('register', [
            'model' => $model,
            'instructionNo' => $instructionNo,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'searchModelOnCheck' => $searchModelOnCheck,
            'dataProviderOnCheck' => $dataProviderOnCheck,
        ]);
    }

    /**
     * Displays a single VehicleCheckInstruction model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VehicleCheckInstruction model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VehicleCheckInstruction();

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            // return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['/vehicle-check-instruction/register']);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing VehicleCheckInstruction model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // Yii::$app->cache->flush();
        // die();
        // if (Yii::$app->request->post()) {
        //     die();
        // }
        if (Yii::$app->request->post('_asnew') == '1') {
            $model = new VehicleCheckInstruction();
            $instructionNo = VehicleCheckInstruction::instructionNo();
         }else{
            $model = $this->findModel($id);
            $instructionNo = $model->instruction_no;
        }
        // var_dump(Yii::$app->request->post());die();
        // if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            // var_dump($model->getErrors());die();
            return $this->render('update', [
                'instructionNo' => $instructionNo,
                'model' => $model,
            ]);
        }
    }

    /**
     * additional method VehicleCheckInstruction model.
     * @param integer $id
     * @return mixed
     */
    public function actionApproveRequest($id)
    {
        $model = VehicleCheckInstruction::findOne($id);

        if($model->load(Yii::$app->request->post()) && $model->save()){
            if(VehicleCheckInstruction::approve($id)){
                Yii::$app->notify->success('Tugasan baru berjaya ditambah.');
                return $this->redirect(['/vehicle-check-instruction/register']);
            } else {
                Yii::$app->notify->fail();
            }
        }
        return $this->render('_approval-form',[
            'model'=>$model
            ]);
    }
    public function actionApprove($id)
    {
        if(VehicleCheckInstruction::approve($id)){
            Yii::$app->notify->success('Tugasan baru berjaya ditambah.');
        } else {
            Yii::$app->notify->fail();
        }
        return $this->redirect(['/vehicle-check-instruction/register']);
    }
    public function actionReject($id)
    {
        if(VehicleCheckInstruction::reject($id)){
            Yii::$app->notify->success();
        } else {
            Yii::$app->notify->fail();
        }

        return $this->redirect(['/vehicle-check-instruction/register']);
    }
    public function actionStartTask($id)
    {
        if(VehicleCheckInstruction::startTask($id)){
            Yii::$app->notify->success();
        } else {
            Yii::$app->notify->fail();
        }
        return $this->redirect(['/vehicle-check-instruction/register']);
    }
    public function actionTaskDone($id)
    {
        if(VehicleCheckInstruction::taskDone($id)){
            Yii::$app->notify->success();
        } else {
            Yii::$app->notify->fail();
        }
        return $this->redirect(['/vehicle-check-instruction/register']);
    }

    /**
     * Deletes an existing VehicleCheckInstruction model.
     * If deletion is successful, the browser will be redirected to the '/vehicle-check-instruction/register' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->deleteWithRelated();

        return $this->redirect(['/vehicle-check-instruction/register']);
    }

    /**
     *
     * Export VehicleCheckInstruction information into PDF format.
     * @param integer $id
     * @return mixed
     */
    public function actionPdf($id) {
        $model = $this->findModel($id);

        $content = $this->renderAjax('_pdf', [
            'model' => $model,
        ]);

        $pdf = new \kartik\mpdf\Pdf([
            'mode' => \kartik\mpdf\Pdf::MODE_CORE,
            'format' => \kartik\mpdf\Pdf::FORMAT_A4,
            'orientation' => \kartik\mpdf\Pdf::ORIENT_PORTRAIT,
            'destination' => \kartik\mpdf\Pdf::DEST_BROWSER,
            'content' => $content,
            'cssFile' => '@vendor/kartik-v/yii2-mpdf/assets/kv-mpdf-bootstrap.min.css',
            'cssInline' => '.kv-heading-1{font-size:18px}',
            'options' => ['title' => \Yii::$app->name],
            'methods' => [
                'SetHeader' => [\Yii::$app->name],
                'SetFooter' => ['{PAGENO}'],
            ]
        ]);

        return $pdf->render();
    }

    /**
    * Creates a new VehicleCheckInstruction model by another data,
    * so user don't need to input all field from scratch.
    * If creation is successful, the browser will be redirected to the 'view' page.
    *
    * @param mixed $id
    * @return mixed
    */
    public function actionSaveAsNew($id) {
        $model = new VehicleCheckInstruction();

        if (Yii::$app->request->post('_asnew') != '1') {
            $model = $this->findModel($id);
        }

        if ($model->loadAll(Yii::$app->request->post()) && $model->saveAll()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('saveAsNew', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Finds the VehicleCheckInstruction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VehicleCheckInstruction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VehicleCheckInstruction::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
