<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('StaffProfile'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Account'),
        'content' => $this->render('_dataAccount', [
            'model' => $model,
            'row' => $model->accounts,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Employment'),
        'content' => $this->render('_dataEmployment', [
            'model' => $model,
            'row' => $model->employments,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Hod'),
        'content' => $this->render('_dataHod', [
            'model' => $model,
            'row' => $model->hods,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Leave Request'),
        'content' => $this->render('_dataLeaveRequest', [
            'model' => $model,
            'row' => $model->leaveRequests,
        ]),
    ],
                [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('User Leave Setting'),
        'content' => $this->render('_dataUserLeaveSetting', [
            'model' => $model,
            'row' => $model->userLeaveSettings,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
