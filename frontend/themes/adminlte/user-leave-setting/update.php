<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\UserLeaveSetting */

$this->title = 'Update User Leave Setting: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Leave Setting', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="user-leave-setting-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
