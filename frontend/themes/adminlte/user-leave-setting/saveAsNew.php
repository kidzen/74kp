<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserLeaveSetting */

$this->title = 'Save As New User Leave Setting: '. ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User Leave Setting', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="user-leave-setting-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
