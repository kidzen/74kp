<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\UserLeaveSetting */

$this->title = 'Create User Leave Setting';
$this->params['breadcrumbs'][] = ['label' => 'User Leave Setting', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-leave-setting-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
