<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\VehicleBreakdownType */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vehicle Breakdown Type', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehicle-breakdown-type-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Vehicle Breakdown Type'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF', 
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerVehicle->totalCount){
    $gridColumnVehicle = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'type0.name',
                'label' => 'Type'
            ],
            'model',
            'plate_no',
            [
                'attribute' => 'task.name',
                'label' => 'Task'
            ],
            [
                'attribute' => 'holder.name',
                'label' => 'Holder'
            ],
            [
                'attribute' => 'rank.name',
                'label' => 'Rank'
            ],
            [
                'attribute' => 'status0.id',
                'label' => 'Status'
            ],
                        'odometer',
            'progress',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerVehicle,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-vehicle']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Vehicle'),
        ],
        'columns' => $gridColumnVehicle
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerVehicleCheckInstruction->totalCount){
    $gridColumnVehicleCheckInstruction = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'instruction_no',
            'description',
            [
                'attribute' => 'department.name',
                'label' => 'Department'
            ],
            [
                'attribute' => 'vehicle.plate_no',
                'label' => 'Vehicle'
            ],
            [
                'attribute' => 'cDriver.username',
                'label' => 'C Driver'
            ],
            [
                'attribute' => 'cTask.name',
                'label' => 'C Task'
            ],
            [
                'attribute' => 'cHolder.name',
                'label' => 'C Holder'
            ],
            [
                'attribute' => 'cRank.name',
                'label' => 'C Rank'
            ],
            [
                'attribute' => 'cStatus.id',
                'label' => 'C Status'
            ],
                        'c_odometer',
            'c_progress',
            'request_at',
            [
                'attribute' => 'requestBy.username',
                'label' => 'Request By'
            ],
            'required_at',
            'checked_at',
            [
                'attribute' => 'checkedBy.username',
                'label' => 'Checked By'
            ],
            'approved',
            [
                'attribute' => 'approvedBy.username',
                'label' => 'Approved By'
            ],
            'approved_at',
            'total_cost',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerVehicleCheckInstruction,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-vehicle-check-instruction']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Vehicle Check Instruction'),
        ],
        'columns' => $gridColumnVehicleCheckInstruction
    ]);
}
?>
    </div>
</div>
