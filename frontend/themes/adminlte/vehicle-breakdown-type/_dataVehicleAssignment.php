<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->vehicleAssignments,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'assignment_no',
        [
                'attribute' => 'vehicle.id',
                'label' => 'Vehicle'
            ],
        // [
        //         'attribute' => 'cDriver.username',
        //         'label' => 'C Driver'
        //     ],
        // [
        //         'attribute' => 'cTask.name',
        //         'label' => 'C Task'
        //     ],
        // [
        //         'attribute' => 'cHolder.name',
        //         'label' => 'C Holder'
        //     ],
        // [
        //         'attribute' => 'cRank.name',
        //         'label' => 'C Rank'
        //     ],
        [
                'attribute' => 'cStatus.id',
                'label' => 'C Status'
            ],
        'c_odometer',
        'c_progress',
        'status',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'vehicle-assignment'
        ],
    ];

    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
