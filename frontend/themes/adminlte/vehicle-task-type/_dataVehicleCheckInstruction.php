<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->vehicleCheckInstructions,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'instruction_no',
        'description',
        [
                'attribute' => 'department.name',
                'label' => 'Department'
            ],
        [
                'attribute' => 'vehicle.plate_no',
                'label' => 'Vehicle'
            ],
        [
                'attribute' => 'cDriver.username',
                'label' => 'C Driver'
            ],
        [
                'attribute' => 'cHolder.name',
                'label' => 'C Holder'
            ],
        [
                'attribute' => 'cRank.name',
                'label' => 'C Rank'
            ],
        [
                'attribute' => 'cStatus.id',
                'label' => 'C Status'
            ],
        [
                'attribute' => 'cBreakdown.name',
                'label' => 'C Breakdown'
            ],
        'c_odometer',
        'c_progress',
        'request_at',
        [
                'attribute' => 'requestBy.username',
                'label' => 'Request By'
            ],
        'required_at',
        'checked_at',
        [
                'attribute' => 'checkedBy.username',
                'label' => 'Checked By'
            ],
        'approved',
        [
                'attribute' => 'approvedBy.username',
                'label' => 'Approved By'
            ],
        'approved_at',
        'total_cost',
        'status',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'vehicle-check-instruction'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
