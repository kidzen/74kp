<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
//
//if (isset(Yii::$app->user->id)) {
//    $online = Yii::$app->user->identity->username;
//    $role = Yii::$app->user->identity->role;
//    $created = 'Member since ' . Yii::$app->user->identity->created_at;
//    if (Yii::$app->user->identity->active()) {
//        $activeBool = true;
//    } else {
//        $activeBool = false;
//    }
//} else {
//    $online = 'Guest';
//    $role = '';
//    $created = '';
//    $activeBool = false;
//}

//echo Yii::$app->user->name.'<br>';
//if (isset(Yii::$app->user->id)){
//$active = '<p style="color: red"><b>You are not an active user..<br>Please kindly ask admin to approve you.</b></p>';
////echo Yii::$app->user->identity->id;
////echo '<br>';
////echo Yii::$app->user->identity->role;
////echo '<br>';
////echo Yii::$app->user->identity->username;
//}
//else {
//    $active = '<p style="color: red"><b>You are not an active user..<br>Please kindly ask admin to approve you.</b></p>';
//}

// $this->title = 'Sistem 74KP';
$this->title = 'Dashboard<small>Version 2.0</small>';
//var_dump(Yii::$app->user->role);
//var_dump(Yii::$app->user->identity->role);
?>

<div class="site-index">

<?php // echo date('Y-m-d H:i:s');?>
    <div class="jumbotron text-center">
        <h1><i><strong>V-Alert System</strong></i></h1>

        <p class="lead"><i>V-Alert System</i> 74 Bn Kor Perkhidmatan Diraja</p>

        <!--<p><a class="btn btn-lg btn-success" href="#">Log Masuk</a></p>-->
          <?= Html::a('Log Masuk',['/site/login'],['class'=>'btn btn-lg btn-success']); ?>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Mengenai Kami</h2>

                <p><strong>Kor Perkhidmatan Diraja (KPD)</strong> berasal dengan penubuhan Cawangan Bekal dan Angkut dalam bulan April 1957 sebagai satu bahagian dalam Army Service Corps (ASC). ASC telah ditukarkan nama kepada Armed Forces Maintainance Corps (AFMC) selaras dengan tugasnya memberi bantuan logistik kepada semua Perkhidmatan Angkatan Tentera. Pada 9 April 1965 AFMC telah dibubarkan dan Pasukan Bekal dan Angkut Tentera telah ditubuhkan. Pasukan ini kemudian dikenali dengan nama Kor Perkhidmatan Diraja dan bercogankata <i><strong>"Berkidmat Kepada Semua"</strong></i>. Ditugaskan sebagai pasukan logistik Tentera Darat seperti Workshop dan kompeni angkut, kecuali senjata dan bekalan asas. </p>

                <p><a class="btn btn-default" href="https://ms.wikipedia.org/wiki/Tentera_Darat_Malaysia#Kor_Perkhidmatan_Diraja_.28KPD.29" target="_blank">Lebih info &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Mengenai Sistem</h2>

                <p><i><strong>Yii2 Framework</strong></i> adalah satu rangka kerja PHP berprestasi tinggi terbaik untuk membangunkan aplikasi Web 2.0.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/" target="_blank">Lebih info &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Angkatan Tentera Malaysia</h2>

                <p><strong>Angkatan Tentera Malaysia (ATM)</strong> ialah pihak yang bertanggungjawab terhadap pertahanan nasional dengan kata lain ialah pasukan tentera profesional negara Malaysia. ATM meliputi tiga cabang ketenteraan utama iaitu; Tentera Darat Malaysia (TDM), Tentera Laut Diraja Malaysia (TLDM) dan Tentera Udara Diraja Malaysia (TUDM). Pemimpin tertinggi Angkatan Tentera Malaysia ialah Panglima Angkatan Tentera YM Jeneral Tan Sri Raja Mohamad Affandi bin Raja Mohamad Noor.</p>

                <p><a class="btn btn-default" href="http://www.mafhq.mil.my" target="_blank">Lebih info &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
