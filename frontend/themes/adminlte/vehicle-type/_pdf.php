<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\VehicleType */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vehicle Type', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehicle-type-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Vehicle Type'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'assetType.name',
                'label' => 'Asset Type'
            ],
        'name',
        'description',
        'engine',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerVehicle->totalCount){
    $gridColumnVehicle = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'model',
        'plate_no',
        [
                'attribute' => 'task.name',
                'label' => 'Task'
            ],
        [
                'attribute' => 'holder.name',
                'label' => 'Holder'
            ],
        [
                'attribute' => 'rank.name',
                'label' => 'Rank'
            ],
        [
                'attribute' => 'status0.id',
                'label' => 'Status'
            ],
        [
                'attribute' => 'breakdown.name',
                'label' => 'Breakdown'
            ],
        'odometer',
        'progress',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerVehicle,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Vehicle'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnVehicle
    ]);
}
?>
    </div>
</div>
