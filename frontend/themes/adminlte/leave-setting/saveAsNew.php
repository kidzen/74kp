<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LeaveSetting */

$this->title = 'Save As New Leave Setting: '. ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Leave Setting', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="leave-setting-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
