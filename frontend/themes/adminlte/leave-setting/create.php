<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\LeaveSetting */

$this->title = 'Create Leave Setting';
$this->params['breadcrumbs'][] = ['label' => 'Leave Setting', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leave-setting-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
