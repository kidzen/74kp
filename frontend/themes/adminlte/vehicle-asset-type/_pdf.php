<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\VehicleAssetType */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vehicle Asset Type', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehicle-asset-type-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Vehicle Asset Type'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerVehicleType->totalCount){
    $gridColumnVehicleType = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'name',
        'description',
        'engine',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerVehicleType,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Vehicle Type'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnVehicleType
    ]);
}
?>
    </div>
</div>
