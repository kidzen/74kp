<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\VehicleHolderType */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Vehicle Holder Type', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehicle-holder-type-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Vehicle Holder Type'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerVehicle->totalCount){
    $gridColumnVehicle = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'type0.name',
                'label' => 'Type'
            ],
        'model',
        'plate_no',
        [
                'attribute' => 'task.name',
                'label' => 'Task'
            ],
                [
                'attribute' => 'rank.name',
                'label' => 'Rank'
            ],
        [
                'attribute' => 'status0.id',
                'label' => 'Status'
            ],
        [
                'attribute' => 'breakdown.name',
                'label' => 'Breakdown'
            ],
        'odometer',
        'progress',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerVehicle,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Vehicle'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnVehicle
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerVehicleAssignment->totalCount){
    $gridColumnVehicleAssignment = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'assignment_no',
        [
                'attribute' => 'vehicle.plate_no',
                'label' => 'Vehicle'
            ],
        [
                'attribute' => 'cDriver.username',
                'label' => 'C Driver'
            ],
        [
                'attribute' => 'cTask.name',
                'label' => 'C Task'
            ],
                [
                'attribute' => 'cRank.name',
                'label' => 'C Rank'
            ],
        [
                'attribute' => 'cStatus.id',
                'label' => 'C Status'
            ],
        'c_odometer',
        'c_progress',
        'request_at',
        [
                'attribute' => 'requestBy.username',
                'label' => 'Request By'
            ],
        'required_at',
        'checked_at',
        'checked_by',
        'approved',
        [
                'attribute' => 'approvedBy.username',
                'label' => 'Approved By'
            ],
        'approved_at',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerVehicleAssignment,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Vehicle Assignment'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnVehicleAssignment
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerVehicleCheckInstruction->totalCount){
    $gridColumnVehicleCheckInstruction = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'instruction_no',
        'description',
        [
                'attribute' => 'department.name',
                'label' => 'Department'
            ],
        [
                'attribute' => 'vehicle.plate_no',
                'label' => 'Vehicle'
            ],
        [
                'attribute' => 'cDriver.username',
                'label' => 'C Driver'
            ],
        [
                'attribute' => 'cTask.name',
                'label' => 'C Task'
            ],
                [
                'attribute' => 'cRank.name',
                'label' => 'C Rank'
            ],
        [
                'attribute' => 'cStatus.id',
                'label' => 'C Status'
            ],
        [
                'attribute' => 'cBreakdown.name',
                'label' => 'C Breakdown'
            ],
        'c_odometer',
        'c_progress',
        'request_at',
        [
                'attribute' => 'requestBy.username',
                'label' => 'Request By'
            ],
        'required_at',
        'checked_at',
        [
                'attribute' => 'checkedBy.username',
                'label' => 'Checked By'
            ],
        'approved',
        [
                'attribute' => 'approvedBy.username',
                'label' => 'Approved By'
            ],
        'approved_at',
        'total_cost',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerVehicleCheckInstruction,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Vehicle Check Instruction'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnVehicleCheckInstruction
    ]);
}
?>
    </div>
</div>
