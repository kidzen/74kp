<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\LeaveRequest */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Leave Request', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leave-request-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Leave Request'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'staff.name',
                'label' => 'Staff'
            ],
        [
                'attribute' => 'leaveType.name',
                'label' => 'Leave Type'
            ],
        'date_from',
        'date_to',
        'request_at',
        'approved',
        'approved_at',
        [
                'attribute' => 'approvedBy.name',
                'label' => 'Approved By'
            ],
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
