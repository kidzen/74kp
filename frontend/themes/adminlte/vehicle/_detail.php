<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Vehicle */

?>
<div class="vehicle-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->plate_no) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'type0.name',
            'label' => 'Type',
        ],
        'model',
        'plate_no',
        [
            'attribute' => 'task.name',
            'label' => 'Task',
        ],
        [
            'attribute' => 'holder.name',
            'label' => 'Holder',
        ],
        [
            'attribute' => 'rank.name',
            'label' => 'Rank',
        ],
        [
            'attribute' => 'status0.id',
            'label' => 'Status',
        ],
        [
            'attribute' => 'breakdown.name',
            'label' => 'Breakdown',
        ],
        'odometer',
        'progress',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>