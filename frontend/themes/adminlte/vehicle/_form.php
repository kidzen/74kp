<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Vehicle */
/* @var $form yii\widgets\ActiveForm */
$detailForm = isset($detailForm) ? $detailForm : 0;

\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
    'class' => 'VehicleAssignment',
    'relID' => 'vehicle-assignment',
    'value' => \yii\helpers\Json::encode($model->vehicleAssignments),
    'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
    ]);
\mootensai\components\JsBlock::widget(['viewFile' => '_script', 'pos'=> \yii\web\View::POS_END,
    'viewParams' => [
    'class' => 'VehicleCheckInstruction',
    'relID' => 'vehicle-check-instruction',
    'value' => \yii\helpers\Json::encode($model->vehicleCheckInstructions),
    'isNewRecord' => ($model->isNewRecord) ? 1 : 0
    ]
    ]);
    ?>

    <div class="vehicle-form">
        <div class="row">

            <?php $form = ActiveForm::begin(); ?>

            <?= $form->errorSummary($model); ?>

            <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>
            <div class="col-md-4">
                <?= $form->field($model, 'type')->widget(\kartik\widgets\Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleType::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                    'options' => ['placeholder' => 'Choose Vehicle type'],
                    'pluginOptions' => [
                    'allowClear' => true
                    ],
                ]); ?>
            </div>
            <div class="col-md-4">

                <?= $form->field($model, 'model')->textInput(['maxlength' => true, 'placeholder' => 'Model']) ?>
            </div>
            <div class="col-md-4">

                <?= $form->field($model, 'plate_no')->textInput(['maxlength' => true, 'placeholder' => 'Plate No']) ?>
            </div>
        <?php if($detailForm == 1){ ?>
            <div class="col-md-4">
                <?= $form->field($model, 'task_id')->widget(\kartik\widgets\Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleTaskType::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                    'options' => ['placeholder' => 'Choose Vehicle task type'],
                    'pluginOptions' => [
                    'allowClear' => true
                    ],
                ]); ?>

            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'holder_id')->widget(\kartik\widgets\Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleHolderType::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                    'options' => ['placeholder' => 'Choose Vehicle holder type'],
                    'pluginOptions' => [
                    'allowClear' => true
                    ],
                ]); ?>

            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'rank_id')->widget(\kartik\widgets\Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleRankType::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                    'options' => ['placeholder' => 'Choose Vehicle rank type'],
                    'pluginOptions' => [
                    'allowClear' => true
                    ],
                ]); ?>

            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'status_id')->widget(\kartik\widgets\Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleStatusType::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                    'options' => ['placeholder' => 'Choose Vehicle status type'],
                    'pluginOptions' => [
                    'allowClear' => true
                    ],
                ]); ?>

            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'breakdown_id')->widget(\kartik\widgets\Select2::classname(), [
                    'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleBreakdownType::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                    'options' => ['placeholder' => 'Choose Vehicle breakdown type'],
                    'pluginOptions' => [
                    'allowClear' => true
                    ],
                ]); ?>

            </div>
        <?php } ?>
            <div class="col-md-4">
                <?= $form->field($model, 'odometer')->textInput(['placeholder' => 'Odometer']) ?>

            </div>
        <?php if($detailForm == 1){ ?>
            <div class="col-md-4">
                <?= $form->field($model, 'progress')->textInput(['placeholder' => 'Progress']) ?>

            </div>
            <div class="col-md-4">
                <?= $form->field($model, 'status')->textInput(['placeholder' => 'Status']) ?>

            </div>
        <?php } ?>
<?php /*
    <?php
    $forms = [
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('VehicleAssignment'),
            'content' => $this->render('_formVehicleAssignment', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->vehicleAssignments),
            ]),
        ],
        [
            'label' => '<i class="glyphicon glyphicon-book"></i> ' . Html::encode('VehicleCheckInstruction'),
            'content' => $this->render('_formVehicleCheckInstruction', [
                'row' => \yii\helpers\ArrayHelper::toArray($model->vehicleCheckInstructions),
            ]),
        ],
    ];
    echo kartik\tabs\TabsX::widget([
        'items' => $forms,
        'position' => kartik\tabs\TabsX::POS_ABOVE,
        'encodeLabels' => false,
        'pluginOptions' => [
            'bordered' => true,
            'sideways' => true,
            'enableCache' => false,
        ],
    ]);
    ?>
*/ ?>
    </div>
    <div class="form-group">
        <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php endif; ?>
        <?php if(Yii::$app->controller->action->id != 'create'): ?>
            <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
        <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>

    </div>
    <?php ActiveForm::end(); ?>

</div>
