<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\VehicleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Vehicle';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="vehicle-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Vehicle', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Advance Search', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'type',
                'label' => 'Type',
                'value' => function($model){
                    if ($model->type0)
                    {return $model->type0->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleType::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Vehicle type', 'id' => 'grid-vehicle-search-type']
            ],
        'model',
        'plate_no',
        [
                'attribute' => 'details',
                'label' => 'Details',
                'format' => 'raw',
                'value' => function($model){
                    $task = isset($model->task)? 'Penugasan :<br>'.$model->task->name.'<br><br>': NULL;
                    $holder = isset($model->holder)? 'Pegangan :<br>'.$model->holder->name.'<br><br>': NULL;
                    $rank = isset($model->rank)? 'Perjawatan :<br>'.$model->rank->name.'<br><br>': NULL;
                    if($task||$holder||$rank){
                        return $task.$holder.$rank;
                    }
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleTaskType::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Vehicle task type', 'id' => 'grid-vehicle-search-task_id']
            ],
        // [
        //         'attribute' => 'task_id',
        //         'label' => 'Task',
        //         'value' => function($model){
        //             return isset($model->task)? $model->task->name: NULL;
        //         },
        //         'filterType' => GridView::FILTER_SELECT2,
        //         'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleTaskType::find()->asArray()->all(), 'id', 'name'),
        //         'filterWidgetOptions' => [
        //             'pluginOptions' => ['allowClear' => true],
        //         ],
        //         'filterInputOptions' => ['placeholder' => 'Vehicle task type', 'id' => 'grid-vehicle-search-task_id']
        //     ],
        // [
        //         'attribute' => 'holder_id',
        //         'label' => 'Holder',
        //         'value' => function($model){
        //             return isset($model->holder)? $model->holder->name: NULL;
        //         },
        //         'filterType' => GridView::FILTER_SELECT2,
        //         'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleHolderType::find()->asArray()->all(), 'id', 'name'),
        //         'filterWidgetOptions' => [
        //             'pluginOptions' => ['allowClear' => true],
        //         ],
        //         'filterInputOptions' => ['placeholder' => 'Vehicle holder type', 'id' => 'grid-vehicle-search-holder_id']
        //     ],
        // [
        //         'attribute' => 'rank_id',
        //         'label' => 'Rank',
        //         'value' => function($model){
        //             return isset($model->rank)? $model->rank->name: NULL;
        //         },
        //         'filterType' => GridView::FILTER_SELECT2,
        //         'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleRankType::find()->asArray()->all(), 'id', 'name'),
        //         'filterWidgetOptions' => [
        //             'pluginOptions' => ['allowClear' => true],
        //         ],
        //         'filterInputOptions' => ['placeholder' => 'Vehicle rank type', 'id' => 'grid-vehicle-search-rank_id']
        //     ],
        [
                'attribute' => 'status_id',
                'label' => 'Code',
                'value' => function($model){
                    if ($model->status0)
                    {return $model->status0->description;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleStatusType::find()->asArray()->all(), 'id', 'id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Vehicle status type', 'id' => 'grid-vehicle-search-status_id']
            ],
        'odometer',
        [
                'attribute' => 'breakdown_id',
                'label' => 'Breakdown',
                'value' => function($model){
                    if ($model->breakdown)
                    {return $model->breakdown->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleBreakdownType::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Vehicle breakdown type', 'id' => 'grid-vehicle-search-breakdown_id']
            ],
        [
                'attribute' => 'progress',
                'label' => 'Progress',
                'format' => 'raw',
                'value' => function($model){
                    $progress = NULL;
                    switch ($model->progress) {
                        case 1:
                            $progress = Html::tag('span','Job Completed',['class'=>'label label-success']);
                            break;
                        case 2:
                            $progress = Html::tag('span','Job Requested',['class'=>'label label-warning']);
                            break;
                        case 3:
                            $progress = Html::tag('span','Ready For Job',['class'=>'label label-primary']);
                            break;
                        case 4:
                            $progress = Html::tag('span','Job Ongoing',['class'=>'label label-info']);
                            break;
                        case 5:
                            $progress = Html::tag('span','Maintenance Completed',['class'=>'label label-success']);
                            break;
                        case 6:
                            $progress = Html::tag('span','Requested For Maintenance',['class'=>'label label-warning']);
                            break;
                        case 7:
                            $progress = Html::tag('span','Ready For Maintenance',['class'=>'label label-primary']);
                            break;
                        case 8:
                            $progress = Html::tag('span','Ongoing Maintenance',['class'=>'label label-info']);
                            break;
                    }
                    $requestedBy = isset($model->requestBy)?'Requested<br>by '.$model->requestBy->username:NULL;
                    $requestedAt = isset($model->request_at)?'<br>At '.$model->request_at:NULL;
                    return $progress.Html::tag('span',$requestedBy.$requestedAt,['class'=>'help-block']);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [1 => 'Job Completed', 2 => 'Job Requested', 3 => 'Ready For Job', 4 => 'Job Ongoing', 5 => 'Maintenance Completed', 6 => 'Requested For Maintenance', 7 => 'Ready For Maintenance', 8 => 'Ongoing Maintenance',],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-vehicle-assignment-search-progress']
            ],
        [
            'attribute'=>'status',
            'format'=>'raw',
            'value'=>function($model) {
                switch($model->status){
                    case 1: return Html::tag('span','Active',['class'=>'label label-success']);break;
                    case 0: return Html::tag('span','Deleted',['class'=>'label label-danger']);break;
                }
            },
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{save-as-new} {view} {update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        // 'responsize' => false,
        'responsiveWrap' => false,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-vehicle']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
            ]) ,
        ],
    ]); ?>

</div>
