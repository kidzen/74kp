<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Vehicle */

$this->title = 'Save As New Vehicle: '. ' ' . $model->plate_no;
$this->params['breadcrumbs'][] = ['label' => 'Vehicle', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->plate_no, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="vehicle-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
