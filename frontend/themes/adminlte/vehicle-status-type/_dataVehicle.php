<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->vehicles,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'type0.name',
                'label' => 'Type'
            ],
        'model',
        'plate_no',
        [
                'attribute' => 'task.name',
                'label' => 'Task'
            ],
        [
                'attribute' => 'holder.name',
                'label' => 'Holder'
            ],
        [
                'attribute' => 'rank.name',
                'label' => 'Rank'
            ],
        [
                'attribute' => 'breakdown.name',
                'label' => 'Breakdown'
            ],
        'odometer',
        'progress',
        'status',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'vehicle'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
