<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PayrollBase */

$this->title = 'Create Payroll Base';
$this->params['breadcrumbs'][] = ['label' => 'Payroll Base', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payroll-base-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
