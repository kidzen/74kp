<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\PayrollBase */

$this->title = 'Update Payroll Base: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payroll Base', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payroll-base-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
