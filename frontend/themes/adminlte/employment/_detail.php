<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Employment */

?>
<div class="employment-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'staff.name',
            'label' => 'Staff',
        ],
        [
            'attribute' => 'employmentType.name',
            'label' => 'Employment Type',
        ],
        [
            'attribute' => 'position.name',
            'label' => 'Position',
        ],
        'date_from',
        'date_to',
        'request_at',
        'approved',
        'approved_at',
        [
            'attribute' => 'approvedBy.name',
            'label' => 'Approved By',
        ],
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>