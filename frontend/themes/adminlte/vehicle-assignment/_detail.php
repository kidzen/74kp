<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\VehicleAssignment */

?>
<div class="vehicle-assignment-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->assignment_no) ?></h2>
        </div>
    </div>

    <div class="row">
<?php
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'assignment_no',
        [
            'attribute' => 'vehicle.plate_no',
            'label' => 'Vehicle',
        ],
        [
            'attribute' => 'cDriver.username',
            'label' => 'C Driver',
        ],
        [
            'attribute' => 'cTask.name',
            'label' => 'C Task',
        ],
        [
            'attribute' => 'cHolder.name',
            'label' => 'C Holder',
        ],
        [
            'attribute' => 'cRank.name',
            'label' => 'C Rank',
        ],
        [
            'attribute' => 'cStatus.id',
            'label' => 'C Status',
        ],
        'c_odometer',
        'c_progress',
        'request_at',
        [
            'attribute' => 'requestBy.username',
            'label' => 'Request By',
        ],
        'required_at',
        'checked_at',
        'checked_by',
        'approved',
        [
            'attribute' => 'approvedBy.username',
            'label' => 'Approved By',
        ],
        'approved_at',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
