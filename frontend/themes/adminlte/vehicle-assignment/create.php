<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VehicleAssignment */

$this->title = 'Create Vehicle Assignment';
$this->params['breadcrumbs'][] = ['label' => 'Vehicle Assignment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehicle-assignment-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
        'assignmentNo' => $assignmentNo,
    ]) ?>

</div>
