<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\VehicleAssignmentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Vehicle Assignment';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$js = "
$('.ajax').click(function(e){
    $.ajax({
       url: 'index.php?r=vehicle-assignment/approve',
       data: {id: 32},
       success: function(data) {
           // process data

       }
    });
    return false;
});
";
$this->registerJs($search);
// $this->registerJs($js);
?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li><a href="#vehicle-assignment-form" data-toggle="tab">Permohonan</a></li>
        <li class="active"><a href="#vehicle-assignment-index" data-toggle="tab">Pengesahan</a></li>
        <li><a href="#vehicle-assignment-on-task" data-toggle="tab">Dalam Tugasan</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" id="vehicle-assignment-form">
            <?= $this->render('_form', [
                'assignmentNo' => $assignmentNo,
                'model' => $model,
            ]) ?>
        </div>
        <div class="active tab-pane" id="vehicle-assignment-index">
            <?= $this->render('_index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]) ?>
        </div>
        <div class="tab-pane" id="vehicle-assignment-on-task">
            <?= $this->render('_index', [
                'searchModel' => $searchModelOnTask,
                'dataProvider' => $dataProviderOnTask,
            ]) ?>
        </div>
    </div>
</div>
