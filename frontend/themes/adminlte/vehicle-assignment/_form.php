<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VehicleAssignment */
/* @var $form yii\widgets\ActiveForm */
$detailForm = isset($detailForm) ? $detailForm : 0;
?>
<div class="vehicle-assignment-form">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->errorSummary($model); ?>
    <div class="row">


        <div class="col-md-4">
            <?= $form->field($model, 'assignment_no')->textInput(['maxlength' => true, 'placeholder' => 'Assignment No','value'=>$assignmentNo, 'readOnly'=>true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'vehicle_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\Vehicle::find()->orderBy('id')->asArray()->all(), 'id', 'plate_no'),
                'options' => ['placeholder' => 'Choose Vehicle'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'c_driver_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'username'),
                'options' => ['placeholder' => 'Choose User'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
    </div>
    <div class="row">
    <?php if($detailForm == 1){ ?>
        <div class="col-md-4">
            <?= $form->field($model, 'c_odometer')->textInput(['placeholder' => 'C Odometer']) ?>
        </div>
        <?php } ?>
        <div class="col-md-4">
            <?= $form->field($model, 'required_at')->widget(\kartik\datecontrol\DateControl::classname(), [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                'saveFormat' => 'php:Y-m-d',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Required At',
                        'autoclose' => true
                    ]
                ],
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'c_task_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleTaskType::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Choose Vehicle task type'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'c_holder_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleHolderType::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Choose Vehicle holder type'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'c_rank_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleRankType::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Choose Vehicle rank type'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
    <?php if($detailForm == 1){ ?>
         <div class="col-md-4" style="display: none">
            <?= $form->field($model, 'c_status_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleStatusType::find()->orderBy('id')->asArray()->all(), 'id', 'code_status','description'),
                'options' => ['placeholder' => 'Choose Vehicle status type'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
    <?php } ?>
    <?php if($detailForm == 1){ ?>
         <div class="col-md-4">
            <?= $form->field($model, 'c_progress')->widget(\kartik\widgets\Select2::classname(), [
                'data' => [2=>'Requested',3=>'Ongoing',1=>'Completed'],
                // 'options' => ['placeholder' => 'Choose progress type'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
         <div class="col-md-4" style="display: none">
            <?= $form->field($model, 'request_at')->widget(\kartik\datecontrol\DateControl::classname(), [
                'type' => \kartik\datecontrol\DateControl::FORMAT_DATE,
                'saveFormat' => 'php:Y-m-d',
                'ajaxConversion' => true,
                'options' => [
                    'pluginOptions' => [
                        'placeholder' => 'Choose Request At',
                        'autoclose' => true
                    ]
                ],
            ]); ?>
        </div>
    <?php } ?>
    </div>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
