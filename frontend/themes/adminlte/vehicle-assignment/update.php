<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VehicleAssignment */

$this->title = 'Update Vehicle Assignment: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vehicle Assignment', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vehicle-assignment-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
        'assignmentNo' => $assignmentNo,
    ]) ?>

</div>
