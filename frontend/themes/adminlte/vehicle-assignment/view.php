<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\VehicleAssignment */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vehicle Assignment', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehicle-assignment-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= 'Vehicle Assignment'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . 'PDF', 
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => 'Will open the generated PDF file in a new window'
                ]
            )?>
            <?= Html::a('Save As New', ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'assignment_no',
        [
            'attribute' => 'vehicle.plate_no',
            'label' => 'Vehicle',
        ],
        [
            'attribute' => 'cDriver.username',
            'label' => 'C Driver',
        ],
        [
            'attribute' => 'cTask.name',
            'label' => 'C Task',
        ],
        [
            'attribute' => 'cHolder.name',
            'label' => 'C Holder',
        ],
        [
            'attribute' => 'cRank.name',
            'label' => 'C Rank',
        ],
        [
            'attribute' => 'cStatus.id',
            'label' => 'C Status',
        ],
        'c_odometer',
        'c_progress',
        'request_at',
        [
            'attribute' => 'requestBy.username',
            'label' => 'Request By',
        ],
        'required_at',
        'checked_at',
        'checked_by',
        'approved',
        [
            'attribute' => 'approvedBy.username',
            'label' => 'Approved By',
        ],
        'approved_at',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
