<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VehicleAssignment */
/* @var $form yii\widgets\ActiveForm */
// $detailForm = isset($detailForm) ? $detailForm : 0;
?>
<div class="vehicle-assignment-form">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->errorSummary($model); ?>
    <div class="row">


        <div class="col-md-4">
            <?= $form->field($model, 'assignment_no')->textInput(['readOnly'=>true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'vehicle_id')->textInput(['readOnly'=>true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'c_driver_id')->textInput(['readOnly'=>true]) ?>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-4">
            <?= $form->field($model, 'c_odometer')->textInput(['placeholder' => 'C Odometer']) ?>
        </div>
    </div>
    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
