<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PayrollPaymentType */

$this->title = 'Create Payroll Payment Type';
$this->params['breadcrumbs'][] = ['label' => 'Payroll Payment Type', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payroll-payment-type-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
