<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PayrollPaymentType */

$this->title = 'Save As New Payroll Payment Type: '. ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payroll Payment Type', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Save As New';
?>
<div class="payroll-payment-type-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
