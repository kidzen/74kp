<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\PayrollPaymentType */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payroll Payment Type', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payroll-payment-type-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Payroll Payment Type'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'payroll_id',
        'payment_type',
        'valid_from',
        'valid_to',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
