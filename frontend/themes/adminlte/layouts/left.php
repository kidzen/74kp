<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->name ?></p>
                <p>Jawatan</p>
                <!-- <a href="#"><i class="fa fa-circle text-success"></i> Role</a> -->
            </div>
        </div>


        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    ['label' => 'Log Masuk', 'icon' => 'sign-in', 'visible' => Yii::$app->user->isGuest,'url' => ['/site/login']],
                    ['label' => 'Halaman Utama','visible' => !Yii::$app->user->isGuest, 'icon' => 'dashboard', 'url' => ['/site/index']],
                    ['label' => 'Permohonan', 'icon' => 'share', 'items' => [
                        // ['label' => 'Cuti', 'icon' => 'share', 'url' => ['/leave-request/create']],
                        ['label' => 'Senarai Kenderaan', 'icon' => 'list', 'url' => ['/vehicle/index']],
                        ['label' => 'Pendaftaran Kenderaan', 'icon' => 'truck', 'url' => ['/vehicle/create']],
                        ['label' => 'Log Kenderaan', 'icon' => 'upload', 'url' => ['/vehicle-assignment/assignment']],
                        ['label' => 'Penyelengaraan', 'icon' => 'wrench', 'url' => ['/vehicle-check-instruction/register']],
                    ],
                    'visible' => !Yii::$app->user->isGuest,
                    ],
                    ['label' => 'Pentadbir', 'icon' => 'dashboard', 'items' => [
                        ['label' => 'Ketua Tadbir', 'icon' => 'users', 'url' => ['/hod/index']],
                        ['label' => 'Modul Cuti', 'icon' => 'envelope', 'items' => [
                            ['label' => 'Pengesahan', 'icon' => 'users', 'url' => ['/leave-request/index']],
                            ['label' => 'Jenis Cuti', 'icon' => 'users', 'url' => ['/leave-type/index']],
                            ['label' => 'Setting', 'icon' => 'users', 'url' => ['/leave-setting/index']],
                        ]],
                        ['label' => 'Modul Gaji', 'icon' => 'users', 'items' => [
                            ['label' => 'Kontrak Staf', 'icon' => 'users', 'url' => ['/employment/index']],
                            ['label' => 'Jenis kontrak', 'icon' => 'users', 'url' => ['/employment-type/index']],
                        ]],
                    ],
                    'visible'=>false,
                    ],
                    ['label' => 'Pentadbiran Sistem Gaji', 'icon' => 'dashboard','items' => [
                        ['label' => 'Profil Saya', 'icon' => 'user', 'url' => ['/staff-profile/index']],
                        ['label' => 'Penyata Gaji', 'icon' => 'file', 'url' => ['/site/index']],
                        ['label' => 'Pengguna', 'icon' => 'users', 'url' => ['/user/index']],
                        ['label' => 'Akses', 'icon' => 'users', 'url' => ['/role/index']],
                        ['label' => 'Jabatan', 'icon' => 'users', 'url' => ['/department/index']],
                        ['label' => 'Jawatan', 'icon' => 'users', 'url' => ['/position/index']],
                    ],
                    'visible'=>false,
                    ],
                    ['label' => 'Pentadbiran Sistem Kenderaan', 'icon' => 'dashboard', 'items' => [
                        ['label' => 'Jenis Asset', 'icon' => 'users', 'url' => ['/vehicle-asset-type/index']],
                        ['label' => 'Jenis Kenderaan', 'icon' => 'users', 'url' => ['/vehicle-type/index']],
                        ['label' => 'Jenis Pegangan', 'icon' => 'users', 'url' => ['/vehicle-holder-type/index']],
                        ['label' => 'Jenis Perjawatan', 'icon' => 'users', 'url' => ['/vehicle-rank-type/index']],
                        ['label' => 'Jenis Penugasan', 'icon' => 'users', 'url' => ['/vehicle-task-type/index']],
                        ['label' => 'Jenis Kod Status', 'icon' => 'users', 'url' => ['/vehicle-status-type/index']],
                    ],
                    'visible' => !Yii::$app->user->isGuest,
                    ],
                    // ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    // ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    // ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    // [
                    //     'label' => 'Same tools',
                    //     'icon' => 'share',
                    //     'url' => '#',
                    //     'items' => [
                    //         ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                    //         ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                    //         [
                    //             'label' => 'Level One',
                    //             'icon' => 'circle-o',
                    //             'url' => '#',
                    //             'items' => [
                    //                 ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                    //                 [
                    //                     'label' => 'Level Two',
                    //                     'icon' => 'circle-o',
                    //                     'url' => '#',
                    //                     'items' => [
                    //                         ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                    //                         ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                    //                     ],
                    //                 ],
                    //             ],
                    //         ],
                    //     ],
                    // ],
                ],
            ]
        ) ?>

    </section>

</aside>
