<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\VehicleCheckInstruction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="vehicle-check-instruction-form">
    <div class="row">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->errorSummary($model); ?>

        <div class="col-md-3">
            <?= $form->field($model, 'instruction_no')->textInput(['maxlength' => true, 'placeholder' => 'Assignment No','value'=>$instructionNo, 'readOnly'=>true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'vehicle_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\Vehicle::find()->orderBy('id')->asArray()->all(), 'id', 'plate_no'),
                'options' => ['placeholder' => 'Choose Vehicle'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>

        </div>
         <div class="col-md-3">
            <?= $form->field($model, 'c_breakdown_id')->widget(\kartik\widgets\Select2::classname(), [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleBreakdownType::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Choose Vehicle breakdown type'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-4">
            <?= $form->field($model, 'description')->textArea() ?>
        </div>
        <div class="clearfix"></div>
    </div>

    <div class="form-group">
    <?php if(Yii::$app->controller->action->id != 'save-as-new'): ?>
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    <?php endif; ?>
    <?php if(Yii::$app->controller->action->id != 'create'): ?>
        <?= Html::submitButton('Save As New', ['class' => 'btn btn-info', 'value' => '1', 'name' => '_asnew']) ?>
    <?php endif; ?>
        <?= Html::a(Yii::t('app', 'Cancel'), Yii::$app->request->referrer , ['class'=> 'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
