<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\VehicleCheckInstructionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Vehicle Check Instruction';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
    $('.search-form').toggle(1000);
    return false;
});";
$js = "
$('.ajax').click(function(e){
    $.ajax({
       url: 'index.php?r=vehicle-assignment/approve',
       data: {id: 32},
       success: function(data) {
           // process data

       }
    });
    return false;
});
";
$this->registerJs($search);
// $this->registerJs($js);

?>
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li><a href="#vehicle-check-instruction-form" data-toggle="tab">Register</a></li>
        <li class="active"><a href="#vehicle-check-instruction-index" data-toggle="tab">Senarai</a></li>
        <li><a href="#vehicle-check-instruction-on-task" data-toggle="tab">Dalam Pemeriksaan</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane" id="vehicle-check-instruction-form">
            <?= $this->render('_form', [
                'instructionNo' => $instructionNo,
                'model' => $model,
            ]) ?>
        </div>
        <div class="active
        tab-pane" id="vehicle-check-instruction-index">
            <?= $this->render('_index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]) ?>
        </div>
        <div class="tab-pane" id="vehicle-check-instruction-on-task">
            <?= $this->render('_index', [
                'searchModel' => $searchModelOnCheck,
                'dataProvider' => $dataProviderOnCheck,
            ]) ?>
        </div>
    </div>
</div>

