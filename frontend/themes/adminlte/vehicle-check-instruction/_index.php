<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\VehicleCheckInstructionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Vehicle Check Instruction';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
	$('.search-form').toggle(1000);
	return false;
});";
$this->registerJs($search);
?>
<div class="vehicle-check-instruction-index">

    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        ['attribute' => 'id', 'visible' => false],
        'instruction_no',
        [
                'attribute' => 'vehicle_id',
                'label' => 'Vehicle',
                'format' => 'raw',
                'value' => function($model){
                    $vehicle = isset($model->vehicle)?Html::tag('div',$model->vehicle->plate_no):NULL;
                    $driver = isset($model->cDriver)?Html::tag('div','Driver : '.$model->cDriver->username):NULL;
                    return $vehicle.$driver;
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\Vehicle::find()->asArray()->all(), 'id', 'plate_no'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Vehicle', 'id' => 'grid-vehicle-assignment-search-vehicle_id']
            ],
        'description',
        // [
        //         'attribute' => 'department_id',
        //         'label' => 'Department',
        //         'value' => function($model){
        //             if ($model->department)
        //             {return $model->department->name;}
        //             else
        //             {return NULL;}
        //         },
        //         'filterType' => GridView::FILTER_SELECT2,
        //         'filter' => \yii\helpers\ArrayHelper::map(\common\models\Department::find()->asArray()->all(), 'id', 'name'),
        //         'filterWidgetOptions' => [
        //             'pluginOptions' => ['allowClear' => true],
        //         ],
        //         'filterInputOptions' => ['placeholder' => 'Department', 'id' => 'grid-vehicle-check-instruction-search-department_id']
        //     ],
        // [
        //         'attribute' => 'c_driver_id',
        //         'label' => 'C Driver',
        //         'value' => function($model){
        //             if ($model->cDriver)
        //             {return $model->cDriver->username;}
        //             else
        //             {return NULL;}
        //         },
        //         'filterType' => GridView::FILTER_SELECT2,
        //         'filter' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->asArray()->all(), 'id', 'username'),
        //         'filterWidgetOptions' => [
        //             'pluginOptions' => ['allowClear' => true],
        //         ],
        //         'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-vehicle-check-instruction-search-c_driver_id']
        //     ],
        // [
        //         'attribute' => 'c_task_id',
        //         'label' => 'C Task',
        //         'value' => function($model){
        //             if ($model->cTask)
        //             {return $model->cTask->name;}
        //             else
        //             {return NULL;}
        //         },
        //         'filterType' => GridView::FILTER_SELECT2,
        //         'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleTaskType::find()->asArray()->all(), 'id', 'name'),
        //         'filterWidgetOptions' => [
        //             'pluginOptions' => ['allowClear' => true],
        //         ],
        //         'filterInputOptions' => ['placeholder' => 'Vehicle task type', 'id' => 'grid-vehicle-check-instruction-search-c_task_id']
        //     ],
        // [
        //         'attribute' => 'c_holder_id',
        //         'label' => 'C Holder',
        //         'value' => function($model){
        //             if ($model->cHolder)
        //             {return $model->cHolder->name;}
        //             else
        //             {return NULL;}
        //         },
        //         'filterType' => GridView::FILTER_SELECT2,
        //         'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleHolderType::find()->asArray()->all(), 'id', 'name'),
        //         'filterWidgetOptions' => [
        //             'pluginOptions' => ['allowClear' => true],
        //         ],
        //         'filterInputOptions' => ['placeholder' => 'Vehicle holder type', 'id' => 'grid-vehicle-check-instruction-search-c_holder_id']
        //     ],
        // [
        //         'attribute' => 'c_rank_id',
        //         'label' => 'C Rank',
        //         'value' => function($model){
        //             if ($model->cRank)
        //             {return $model->cRank->name;}
        //             else
        //             {return NULL;}
        //         },
        //         'filterType' => GridView::FILTER_SELECT2,
        //         'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleRankType::find()->asArray()->all(), 'id', 'name'),
        //         'filterWidgetOptions' => [
        //             'pluginOptions' => ['allowClear' => true],
        //         ],
        //         'filterInputOptions' => ['placeholder' => 'Vehicle rank type', 'id' => 'grid-vehicle-check-instruction-search-c_rank_id']
        //     ],
        // [
        //         'attribute' => 'c_status_id',
        //         'label' => 'C Status',
        //         'value' => function($model){
        //             if ($model->cStatus)
        //             {return $model->cStatus->id;}
        //             else
        //             {return NULL;}
        //         },
        //         'filterType' => GridView::FILTER_SELECT2,
        //         'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleStatusType::find()->asArray()->all(), 'id', 'id'),
        //         'filterWidgetOptions' => [
        //             'pluginOptions' => ['allowClear' => true],
        //         ],
        //         'filterInputOptions' => ['placeholder' => 'Vehicle status type', 'id' => 'grid-vehicle-check-instruction-search-c_status_id']
        //     ],
        [
                'attribute' => 'c_breakdown_id',
                'label' => 'Breakdown',
                'value' => function($model){
                    if ($model->cBreakdown)
                    {return $model->cBreakdown->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleBreakdownType::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Vehicle breakdown type', 'id' => 'grid-vehicle-check-instruction-search-c_breakdown_id']
            ],
        'c_odometer',
        [
                'attribute' => 'c_progress',
                'label' => 'Progress',
                'format' => 'raw',
                'value' => function($model){
                    switch ($model->c_progress) {
                        case 1:
                            $progress = Html::tag('span','Job Completed',['class'=>'label label-success']);
                            break;
                        case 2:
                            $progress = Html::tag('span','Job Requested',['class'=>'label label-warning']);
                            break;
                        case 3:
                            $progress = Html::tag('span','Ready For Job',['class'=>'label label-primary']);
                            break;
                        case 4:
                            $progress = Html::tag('span','Job Ongoing',['class'=>'label label-info']);
                            break;
                        case 5:
                            $progress = Html::tag('span','Maintenance Completed',['class'=>'label label-success']);
                            break;
                        case 6:
                            $progress = Html::tag('span','Requested For Maintenance',['class'=>'label label-warning']);
                            break;
                        case 7:
                            $progress = Html::tag('span','Ready For Maintenance',['class'=>'label label-primary']);
                            break;
                        case 8:
                            $progress = Html::tag('span','Ongoing Maintenance',['class'=>'label label-info']);
                            break;
                    }
                    if ($model->checked_at) {
                            $checked = Html::tag('span','Checked',['class'=>'label label-success']);
                    } else {
                            $checked = Html::tag('span','Waiting for checking',['class'=>'label label-warning']);
                    }
                    $requestBy = isset($model->requestBy)?'By '.$model->requestBy->username:NULL;
                    $requestAt = isset($model->request_at)?'<br>At ' .$model->request_at:NULL;
                    $request = $progress.Html::tag('span',$requestBy . $requestAt,['class'=>'help-block']);

                    $checkedBy = isset($model->checkedBy)?'By '.$model->checkedBy->username:NULL;
                    $checkedAt = isset($model->checked_at)?'<br>At ' .$model->checked_at:NULL;
                    $check = $checked.Html::tag('span',$checkedBy . $checkedAt,['class'=>'help-block']);
                    // return $progress.Html::tag('span',$requestedBy.$requestedAt,['class'=>'help-block']);
                    return Html::tag('div',$request . $check,['class'=>'help-block']);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => [1=>'Completed',2=>'Requested',3=>'Ongoing'],
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-vehicle-assignment-search-c_progress']
            ],
        // 'request_at',
        'required_at',
        // 'checked_at',
        // [
        //         'attribute' => 'checked_by',
        //         'label' => 'Checked By',
        //         'value' => function($model){
        //             if ($model->checkedBy)
        //             {return $model->checkedBy->username;}
        //             else
        //             {return NULL;}
        //         },
        //         'filterType' => GridView::FILTER_SELECT2,
        //         'filter' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->asArray()->all(), 'id', 'username'),
        //         'filterWidgetOptions' => [
        //             'pluginOptions' => ['allowClear' => true],
        //         ],
        //         'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-vehicle-check-instruction-search-checked_by']
        //     ],
        [
                'attribute' => 'approved',
                'label' => 'Approved',
                'format' => 'raw',
                'value' => function($model){
                    switch($model->approved){
                        case 1: $approved = Html::tag('span','Approved',['class'=>'label label-success']);break;
                        case 2: $approved = Html::tag('span','Pending',['class'=>'label label-warning']);break;
                        case 3: $approved = Html::tag('span','Rejected',['class'=>'label label-danger']);break;
                    }
                    $approvedBy = isset($model->approved_by)?'By '.$model->approvedBy->username:NULL;
                    $approvedAt = isset($model->approved_at)?'<br>at ' .$model->approved_at:NULL;
                    return $approved . Html::tag('span',$approvedBy . $approvedAt,['class'=>'help-block']);
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->asArray()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-vehicle-assignment-search-approved']
            ],
        'total_cost',
        [
            'attribute'=>'status',
            'format'=>'raw',
            'value'=>function($model) {
                switch($model->status){
                    case 1: return Html::tag('span','Active',['class'=>'label label-success']);break;
                    case 0: return Html::tag('span','Deleted',['class'=>'label label-danger']);break;
                }
            },
        ],
        [
            'class' => 'kartik\grid\ActionColumn',
            'dropdown' => true,
            // 'dropdownOptions' => [
            //     // 'class' => 'text-center',
            //     // 'width' => '10px'
            // ],
            'template' => '{approve} {reject} {start-task} {task-done}',
            'buttons' => [
                'start-task' => function ($url,$model) {
                    return Html::tag('div',Html::a('Start', ['start-task','id'=>$model->id], ['title' => 'Start maintenance','class'=>'btn btn-info btn-sm','data-method' => 'POST','data-pjax'=>1,]),['class'=>'text-center']);
                },
                'task-done' => function ($url,$model) {
                    return Html::tag('div',Html::a('Completed', ['task-done','id'=>$model->id], ['title' => 'Completed','class'=>'btn btn-success btn-sm','data-method' => 'POST','data-pjax'=>1,]),['class'=>'text-center']);
                },
                'approve' => function ($url,$model) {
                    return Html::tag('div',Html::a('Approve', ['approve-request','id'=>$model->id], ['title' => 'Approve','class'=>'btn btn-primary btn-sm','data-pjax'=>0,]),['class'=>'text-center']);
                },
                'reject' => function ($url,$model) {
                    return Html::tag('div',Html::a('Reject', ['reject','id'=>$model->id], ['title' => 'Reject','class'=>'btn btn-danger btn-sm','data-method' => 'POST','data-pjax'=>1,]),['class'=>'text-center']);
                },
            ],
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{save-as-new} {view} {update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'responsiveWrap' => false,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-vehicle-check-instruction']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            Html::a('Search', '#', ['class' => 'btn btn-info search-button']),
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
            ]) ,
        ],
    ]); ?>

</div>
