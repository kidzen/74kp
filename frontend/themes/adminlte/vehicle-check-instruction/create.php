<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\VehicleCheckInstruction */

$this->title = 'Create Vehicle Check Instruction';
$this->params['breadcrumbs'][] = ['label' => 'Vehicle Check Instruction', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="vehicle-check-instruction-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
