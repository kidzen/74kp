<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\VehicleCheckInstruction */

$this->title = 'Update Vehicle Check Instruction: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Vehicle Check Instruction', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="vehicle-check-instruction-update">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
        'instructionNo' => $instructionNo,
         ]) ?>

</div>
