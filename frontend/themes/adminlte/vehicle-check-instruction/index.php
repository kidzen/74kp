<?php

/* @var $this yii\web\View */
/* @var $searchModel common\models\search\VehicleCheckInstructionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\helpers\Html;
use kartik\export\ExportMenu;
use kartik\grid\GridView;

$this->title = 'Vehicle Check Instruction';
$this->params['breadcrumbs'][] = $this->title;
$search = "$('.search-button').click(function(){
    $('.search-form').toggle(1000);
    return false;
});";
$this->registerJs($search);
?>
<div class="vehicle-check-instruction-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Vehicle Check Instruction', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Advance Search', '#', ['class' => 'btn btn-info search-button']) ?>
    </p>
    <div class="search-form" style="display:none">
        <?=  $this->render('_search', ['model' => $searchModel]); ?>
    </div>
    <?php
    $gridColumn = [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'class' => 'kartik\grid\ExpandRowColumn',
            'width' => '50px',
            'value' => function ($model, $key, $index, $column) {
                return GridView::ROW_COLLAPSED;
            },
            'detail' => function ($model, $key, $index, $column) {
                return Yii::$app->controller->renderPartial('_expand', ['model' => $model]);
            },
            'headerOptions' => ['class' => 'kartik-sheet-style'],
            'expandOneOnly' => true
        ],
        ['attribute' => 'id', 'visible' => false],
        'instruction_no',
        'description',
        [
                'attribute' => 'department_id',
                'label' => 'Department',
                'value' => function($model){
                    if ($model->department)
                    {return $model->department->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\Department::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Department', 'id' => 'grid-vehicle-check-instruction-search-department_id']
            ],
        [
                'attribute' => 'vehicle_id',
                'label' => 'Vehicle',
                'value' => function($model){
                    if ($model->vehicle)
                    {return $model->vehicle->plate_no;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\Vehicle::find()->asArray()->all(), 'id', 'plate_no'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Vehicle', 'id' => 'grid-vehicle-check-instruction-search-vehicle_id']
            ],
        [
                'attribute' => 'c_driver_id',
                'label' => 'C Driver',
                'value' => function($model){
                    if ($model->cDriver)
                    {return $model->cDriver->username;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->asArray()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-vehicle-check-instruction-search-c_driver_id']
            ],
        [
                'attribute' => 'c_task_id',
                'label' => 'C Task',
                'value' => function($model){
                    if ($model->cTask)
                    {return $model->cTask->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleTaskType::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Vehicle task type', 'id' => 'grid-vehicle-check-instruction-search-c_task_id']
            ],
        [
                'attribute' => 'c_holder_id',
                'label' => 'C Holder',
                'value' => function($model){
                    if ($model->cHolder)
                    {return $model->cHolder->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleHolderType::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Vehicle holder type', 'id' => 'grid-vehicle-check-instruction-search-c_holder_id']
            ],
        [
                'attribute' => 'c_rank_id',
                'label' => 'C Rank',
                'value' => function($model){
                    if ($model->cRank)
                    {return $model->cRank->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleRankType::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Vehicle rank type', 'id' => 'grid-vehicle-check-instruction-search-c_rank_id']
            ],
        [
                'attribute' => 'c_status_id',
                'label' => 'C Status',
                'value' => function($model){
                    if ($model->cStatus)
                    {return $model->cStatus->id;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleStatusType::find()->asArray()->all(), 'id', 'id'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Vehicle status type', 'id' => 'grid-vehicle-check-instruction-search-c_status_id']
            ],
        [
                'attribute' => 'c_breakdown_id',
                'label' => 'C Breakdown',
                'value' => function($model){
                    if ($model->cBreakdown)
                    {return $model->cBreakdown->name;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\VehicleBreakdownType::find()->asArray()->all(), 'id', 'name'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'Vehicle breakdown type', 'id' => 'grid-vehicle-check-instruction-search-c_breakdown_id']
            ],
        'c_odometer',
        'c_progress',
        'request_at',
        [
                'attribute' => 'request_by',
                'label' => 'Request By',
                'value' => function($model){
                    if ($model->requestBy)
                    {return $model->requestBy->username;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->asArray()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-vehicle-check-instruction-search-request_by']
            ],
        'required_at',
        'checked_at',
        [
                'attribute' => 'checked_by',
                'label' => 'Checked By',
                'value' => function($model){
                    if ($model->checkedBy)
                    {return $model->checkedBy->username;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->asArray()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-vehicle-check-instruction-search-checked_by']
            ],
        'approved',
        [
                'attribute' => 'approved_by',
                'label' => 'Approved By',
                'value' => function($model){
                    if ($model->approvedBy)
                    {return $model->approvedBy->username;}
                    else
                    {return NULL;}
                },
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->asArray()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'pluginOptions' => ['allowClear' => true],
                ],
                'filterInputOptions' => ['placeholder' => 'User', 'id' => 'grid-vehicle-check-instruction-search-approved_by']
            ],
        'approved_at',
        'total_cost',
        'status',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{save-as-new} {view} {update} {delete}',
            'buttons' => [
                'save-as-new' => function ($url) {
                    return Html::a('<span class="glyphicon glyphicon-copy"></span>', $url, ['title' => 'Save As New']);
                },
            ],
        ],
    ];
    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumn,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-vehicle-check-instruction']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span>  ' . Html::encode($this->title),
        ],
        // your toolbar can include the additional full export menu
        'toolbar' => [
            '{export}',
            ExportMenu::widget([
                'dataProvider' => $dataProvider,
                'columns' => $gridColumn,
                'target' => ExportMenu::TARGET_BLANK,
                'fontAwesome' => true,
                'dropdownOptions' => [
                    'label' => 'Full',
                    'class' => 'btn btn-default',
                    'itemsBefore' => [
                        '<li class="dropdown-header">Export All Data</li>',
                    ],
                ],
            ]) ,
        ],
    ]); ?>

</div>
