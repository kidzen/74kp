<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\VehicleCheckInstruction */

?>
<div class="vehicle-check-instruction-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'instruction_no',
        'description',
        [
            'attribute' => 'department.name',
            'label' => 'Department',
        ],
        [
            'attribute' => 'vehicle.plate_no',
            'label' => 'Vehicle',
        ],
        [
            'attribute' => 'cDriver.username',
            'label' => 'C Driver',
        ],
        [
            'attribute' => 'cTask.name',
            'label' => 'C Task',
        ],
        [
            'attribute' => 'cHolder.name',
            'label' => 'C Holder',
        ],
        [
            'attribute' => 'cRank.name',
            'label' => 'C Rank',
        ],
        [
            'attribute' => 'cStatus.id',
            'label' => 'C Status',
        ],
        [
            'attribute' => 'cBreakdown.name',
            'label' => 'C Breakdown',
        ],
        'c_odometer',
        'c_progress',
        'request_at',
        [
            'attribute' => 'requestBy.username',
            'label' => 'Request By',
        ],
        'required_at',
        'checked_at',
        [
            'attribute' => 'checkedBy.username',
            'label' => 'Checked By',
        ],
        'approved',
        [
            'attribute' => 'approvedBy.username',
            'label' => 'Approved By',
        ],
        'approved_at',
        'total_cost',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>