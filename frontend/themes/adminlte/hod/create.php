<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Hod */

$this->title = 'Create Hod';
$this->params['breadcrumbs'][] = ['label' => 'Hod', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="hod-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
