<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('User'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
        [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Staff Profile'),
        'content' => $this->render('_dataStaffProfile', [
            'model' => $model,
            'row' => $model->staffProfiles,
        ]),
    ],
                [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Vehicle Assignment'),
        'content' => $this->render('_dataVehicleAssignment', [
            'model' => $model,
            'row' => $model->vehicleAssignments,
        ]),
    ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Vehicle Check Instruction'),
        'content' => $this->render('_dataVehicleCheckInstruction', [
            'model' => $model,
            'row' => $model->vehicleCheckInstructions,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
