<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->username;
$this->params['breadcrumbs'][] = ['label' => 'User', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'User'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'username',
        [
                'attribute' => 'role.name',
                'label' => 'Role'
            ],
        'auth_key',
        'password_hash',
        'password_reset_token',
        'email:email',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerStaffProfile->totalCount){
    $gridColumnStaffProfile = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                'name',
        'nickname',
        'staff_no',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerStaffProfile,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Staff Profile'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnStaffProfile
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerVehicleAssignment->totalCount){
    $gridColumnVehicleAssignment = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'assignment_no',
        [
                'attribute' => 'vehicle.plate_no',
                'label' => 'Vehicle'
            ],
                [
                'attribute' => 'cTask.name',
                'label' => 'C Task'
            ],
        [
                'attribute' => 'cHolder.name',
                'label' => 'C Holder'
            ],
        [
                'attribute' => 'cRank.name',
                'label' => 'C Rank'
            ],
        [
                'attribute' => 'cStatus.id',
                'label' => 'C Status'
            ],
        'c_odometer',
        'c_progress',
        'request_at',
                'required_at',
        'checked_at',
        'checked_by',
        'approved',
                'approved_at',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerVehicleAssignment,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Vehicle Assignment'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnVehicleAssignment
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerVehicleCheckInstruction->totalCount){
    $gridColumnVehicleCheckInstruction = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'instruction_no',
        'description',
        [
                'attribute' => 'department.name',
                'label' => 'Department'
            ],
        [
                'attribute' => 'vehicle.plate_no',
                'label' => 'Vehicle'
            ],
                [
                'attribute' => 'cTask.name',
                'label' => 'C Task'
            ],
        [
                'attribute' => 'cHolder.name',
                'label' => 'C Holder'
            ],
        [
                'attribute' => 'cRank.name',
                'label' => 'C Rank'
            ],
        [
                'attribute' => 'cStatus.id',
                'label' => 'C Status'
            ],
        [
                'attribute' => 'cBreakdown.name',
                'label' => 'C Breakdown'
            ],
        'c_odometer',
        'c_progress',
        'request_at',
                'required_at',
        'checked_at',
                'approved',
                'approved_at',
        'total_cost',
        'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerVehicleCheckInstruction,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode('Vehicle Check Instruction'),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnVehicleCheckInstruction
    ]);
}
?>
    </div>
</div>
