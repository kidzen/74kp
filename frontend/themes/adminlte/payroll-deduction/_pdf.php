<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\PayrollDeduction */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Payroll Deduction', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payroll-deduction-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Payroll Deduction'.' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'employment_id',
        'valid_from',
        'valid_to',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
