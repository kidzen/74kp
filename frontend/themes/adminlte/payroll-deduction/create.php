<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PayrollDeduction */

$this->title = 'Create Payroll Deduction';
$this->params['breadcrumbs'][] = ['label' => 'Payroll Deduction', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payroll-deduction-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
