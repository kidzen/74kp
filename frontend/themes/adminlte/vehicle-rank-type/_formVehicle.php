<div class="form-group" id="add-vehicle">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Vehicle',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'type' => [
            'label' => 'Vehicle type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleType::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Choose Vehicle type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'model' => ['type' => TabularForm::INPUT_TEXT],
        'plate_no' => ['type' => TabularForm::INPUT_TEXT],
        'task_id' => [
            'label' => 'Vehicle task type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleTaskType::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Choose Vehicle task type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'holder_id' => [
            'label' => 'Vehicle holder type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleHolderType::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Choose Vehicle holder type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'status_id' => [
            'label' => 'Vehicle status type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleStatusType::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
                'options' => ['placeholder' => 'Choose Vehicle status type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'breakdown_id' => [
            'label' => 'Vehicle breakdown type',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\common\models\VehicleBreakdownType::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => 'Choose Vehicle breakdown type'],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'odometer' => ['type' => TabularForm::INPUT_TEXT],
        'progress' => ['type' => TabularForm::INPUT_TEXT],
        'status' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return
                    Html::hiddenInput('Children[' . $key . '][id]', (!empty($model['id'])) ? $model['id'] : "") .
                    Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  'Delete', 'onClick' => 'delRowVehicle(' . $key . '); return false;', 'id' => 'vehicle-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . 'Add Vehicle', ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowVehicle()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

