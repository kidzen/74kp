<?php

namespace common\components;

Class Enum {

	public static $progress = [
		1 => 'Job Completed',
		2 => 'Job Requested',
		3 => 'Ready For Job',
		4 => 'Job Ongoing',
		5 => 'Maintenance Completed',
		6 => 'Requested For Maintenance',
		7 => 'Ready For Maintenance',
		8 => 'Ongoing Maintenance',
	];
	public static $monthList = [
		1 => 'Januari',
		2 => 'Febuari',
		3 => 'Mac',
		4 => 'April',
		5 => 'Mei',
		6 => 'Jun',
		7 => 'Julai',
		8 => 'Ogos',
		9 => 'September',
		10 => 'Oktober',
		11 => 'November',
		12 => 'Disember',
	];
	public static $code_status = [
		1 => ['B','Kerosakan memerlukan pembaikan di pasukan'],
		2 => ['C','Kerosakan memerlukan pembaikan di Workshop JLJ'],
		3 => ['W','Kenderaan/Peralatan dalam Workshop JLJ'],
		4 => ['M','Membuat rawatan dan senggaraan servis'],
		5 => ['X','Pemeriksaan Teknikal atau Bulanan BAT M 60/61'],
		6 => ['(M)','Rawatan/Senggaraan telah disiapkan'],
		7 => ['(X)','Pemeriksaan telah disiapkan'],
		8 => ['0','Kenderaan/Peralatan boleh digerakan'],
		9 => ['TBG','Kenderaan belum diselenggara'],
		10 => ['TBW','Kenderaan menghampiri waktu selenggara'],
	];

	public static $kewpsFormList = [
		3 => 'KEW.PS-3 ()',
		4 => 'KEW.PS-4 (KAD PETAK)',
		5 => 'KEW.PS-5 (SENARAI DAFTAR KAD KAWALAN STOK)',
		7 => 'KEW.PS-7 (PENENTUAN KUMPULAN STOK)',
		8 => 'KEW.PS-8 (LABEL FIFO)',
		9 => 'KEW.PS-9 (SENARAI STOK BERTARIKH LUPUT)',
		10 => 'KEW.PS-10 (BORANG PESANAN DAN PENGELUARAN STOK)',
		11 => 'KEW.PS-11 (BORANG PEMESANAN STOK)',
		13 => 'KEW.PS-13 (LAPORAN KEDUDUKAN STOK)',
		14 => 'KEW.PS-14 (LAPORAN PEMERIKSAAN / VERIFIKASI STOR)',
		17 => 'KEW.PS-17 (PENYATA PELARASAN STOK)',
		18 => 'KEW.PS-18 (PERAKUAN AMBIL ALIH)',
	];

	public static $kewpaformList = [
		9 => 'KEW.PA-9 ()',
	];

	public static $approvalType = [
		1 => 'Pending',
		2 => 'Approved',
		3 => 'Rejected',
	];

	public static $status = [
		0 => 'Active',
		1 => 'Deleted',
	];


}
