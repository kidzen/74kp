<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

class User extends \yii\web\User {

    public function getStatus() {
        $identity = $this->getIdentity();

        return $identity !== null ? 'Online' : 'Offline';
    }
    public function getAvatar() {
        $identity = $this->getIdentity();

        return $identity !== null ? $identity->getAvatar() : null;
    }
//    public function getStaffDetail() {
//        $identity = $this->getIdentity();
//
//        return $identity !== null ? $identity->getStaffDetails() : null;
//    }
    public function getUsername() {
        $identity = $this->getIdentity();

        return $identity !== null ? $identity->getUsername() : null;
    }
    public function getName() {
        $identity = $this->getIdentity();
        return $identity !== null ? $identity->getName() : null;
    }

    public function getRole() {
        $identity = $this->getIdentity();

        return $identity !== null ? $identity->getRole() : null;
    }

    public function getIsDev() {
        $identity = $this->getRole();

        return $identity === 'Developer' ? true : false;
    }

    public function getIsAdmin() {
        $identity = $this->getRole();

        return $identity === 'Administrator' ? true : false;
    }

    public function getIsCo() {
        $identity = $this->getRole();

        return $identity === 'CO' ? true : false;
    }

    public function getIsUser() {
        $identity = $this->getRole();

        return $identity === 'User' ? true : false;
    }

    public function getIsOc() {
        $identity = $this->getRole();

        return $identity === 'OC' ? true : false;
    }

    public function getIsPltnSjn() {
        $identity = $this->getRole();

        return $identity === 'Platun Sarjan' ? true : false;
    }

    public function getIsDbk() {
        $identity = $this->getRole();

        return $identity === 'Ketua DBK' ? true : false;
    }

    public function getIsMto() {
        $identity = $this->getRole();

        return $identity === 'MTO' ? true : false;
    }


//    public function getIdentity($autoRenew = true) {
//        if ($this->_identity === false) {
//            if ($this->enableSession && $autoRenew) {
//                $this->_identity = null;
//                $this->renewAuthStatus();
//            } else {
//                return null;
//            }
//        }
//
//        return $this->_identity;
//    }
}
