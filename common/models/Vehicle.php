<?php

namespace common\models;

use \common\models\base\Vehicle as BaseVehicle;

/**
 * This is the model class for table "vehicle".
 */
class Vehicle extends BaseVehicle
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['type', 'task_id', 'holder_id', 'rank_id', 'status_id', 'breakdown_id', 'progress', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['plate_no'], 'required'],
            [['odometer'], 'number'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['model', 'plate_no'], 'string', 'max' => 255],
            [['plate_no'], 'unique']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'model' => 'Model',
            'plate_no' => 'Plate No',
            'task_id' => 'Task ID',
            'holder_id' => 'Holder ID',
            'rank_id' => 'Rank ID',
            'status_id' => 'Status ID',
            'breakdown_id' => 'Breakdown ID',
            'odometer' => 'Odometer',
            'progress' => 'Progress',
            'status' => 'Status',
        ];
    }

    public function sendEmail($model,$mail)
    {
        $mailset['title'] = 'Permohonan Penugasan '.$model->assignment_no.' ('.$model->vehicle_id.') diperlukan pada ' . date('d M Y H:i\H',strtotime($model->required_at));
        // $mailset['content'] = 'Permohonan '.$model->type.' '.Yii::$app->user->name.' pada '.$model->start_date. ' sehingga ' .$model->end_date.' bertujuan '.$model->description.'. '. $model->content;
        // var_dump($this->required_at);die();
        foreach ($mail as $i => $receiver) {
            Yii::$app->mailer->compose(
                ['html' => 'Assignment-html', 'text' => 'Assignment-text'],
                ['receiver' => $receiver],
                ['model' => $model]
                )
            ->setTo($receiver->email)
            ->setFrom($model->mailto)
            ->setSubject($mailset['title'])
            ->send();
        }

        return true;
    }

    // public function afterSave($insert, $changedAttributes) {
    //     parent::afterSave($insert, $changedAttributes);
    //     if($insert){
    //         Yii::$app->notify->success('Kenderaan baru berjaya ditambah.');
    //     }
    //     if($this->odometer >= 100){
    //         $lastService = VehicleCheckInstruction::find()
    //         ->where(['vehicle_id'=>$this->vehicle_id])
    //         ->andWhere(['c_progress'=>1])//Job Completed
    //         ->asArray()
    //         ->select('required_at');
    //         var_dump($lastService);die();
    //         $vehicle = Vehicle::findOne($this->vehicle_id);
    //         $vehicle->odometer = isset($total_meter)?$total_meter:0;
    //         $vehicle->task_id = $this->c_task_id;
    //         $vehicle->holder_id = $this->c_holder_id;
    //         $vehicle->rank_id = $this->c_rank_id;
    //         // $vehicle->status_id = $this->c_status_id;
    //         $vehicle->progress = $this->c_progress;
    //         return $vehicle->save();
    //     }
}
