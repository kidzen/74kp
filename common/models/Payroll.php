<?php

namespace common\models;

use \common\models\base\Payroll as BasePayroll;

/**
 * This is the model class for table "payroll".
 */
class Payroll extends BasePayroll
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['employment_id', 'payment_type', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['valid_from', 'valid_to', 'deleted_at', 'created_at', 'updated_at'], 'safe']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'employment_id' => 'Employment ID',
            'payment_type' => 'Payment Type',
            'valid_from' => 'Valid From',
            'valid_to' => 'Valid To',
            'status' => 'Status',
        ];
    }
}
