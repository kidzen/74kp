<?php

namespace common\models;

use \common\models\base\VehicleStatusType as BaseVehicleStatusType;

/**
 * This is the model class for table "vehicle_status_type".
 */
class VehicleStatusType extends BaseVehicleStatusType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['code_status', 'description'], 'string', 'max' => 255]
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'code_status' => 'Code Status',
            'description' => 'Description',
            'status' => 'Status',
        ];
    }
}
