<?php

namespace common\models;

use \common\models\base\Employment as BaseEmployment;

/**
 * This is the model class for table "employment".
 */
class Employment extends BaseEmployment
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['staff_id', 'employment_type', 'position_id', 'approved_by', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['date_from', 'date_to', 'request_at', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['approved'], 'number']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'staff_id' => 'Staff ID',
            'employment_type' => 'Employment Type',
            'position_id' => 'Position ID',
            'date_from' => 'Date From',
            'date_to' => 'Date To',
            'request_at' => 'Request At',
            'approved' => 'Approved',
            'approved_at' => 'Approved At',
            'approved_by' => 'Approved By',
            'status' => 'Status',
        ];
    }
}
