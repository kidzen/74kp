<?php

namespace common\models;

use \common\models\base\PayrollDeduction as BasePayrollDeduction;

/**
 * This is the model class for table "payroll_deduction".
 */
class PayrollDeduction extends BasePayrollDeduction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['employment_id', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['valid_from', 'valid_to', 'deleted_at', 'created_at', 'updated_at'], 'safe']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'employment_id' => 'Employment ID',
            'valid_from' => 'Valid From',
            'valid_to' => 'Valid To',
            'status' => 'Status',
        ];
    }
}
