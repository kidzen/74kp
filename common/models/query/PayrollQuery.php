<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\query\Payroll]].
 *
 * @see \common\models\query\Payroll
 */
class PayrollQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\query\Payroll[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\Payroll|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}