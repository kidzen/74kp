<?php

namespace common\models;

use \common\models\base\Account as BaseAccount;

/**
 * This is the model class for table "account".
 */
class Account extends BaseAccount
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['staff_id', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['bank_name', 'account_no'], 'required'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['bank_branch', 'bank_name', 'account_no'], 'string', 'max' => 255]
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'staff_id' => 'Staff ID',
            'bank_branch' => 'Bank Branch',
            'bank_name' => 'Bank Name',
            'account_no' => 'Account No',
            'status' => 'Status',
        ];
    }
}
