<?php

namespace common\models;

use \common\models\base\PayrollPaymentType as BasePayrollPaymentType;

/**
 * This is the model class for table "payroll_payment_type".
 */
class PayrollPaymentType extends BasePayrollPaymentType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['payroll_id', 'payment_type', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['valid_from', 'valid_to', 'deleted_at', 'created_at', 'updated_at'], 'safe']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'payroll_id' => 'Payroll ID',
            'payment_type' => 'Payment Type',
            'valid_from' => 'Valid From',
            'valid_to' => 'Valid To',
            'status' => 'Status',
        ];
    }
}
