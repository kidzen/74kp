<?php

namespace common\models;

use \common\models\base\Hod as BaseHod;

/**
 * This is the model class for table "hod".
 */
class Hod extends BaseHod
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['staff_id', 'department_id'], 'required'],
            [['staff_id', 'department_id', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['validity_start', 'validity_end', 'deleted_at', 'created_at', 'updated_at'], 'safe']
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'staff_id' => 'Staff ID',
            'department_id' => 'Department ID',
            'validity_start' => 'Validity Start',
            'validity_end' => 'Validity End',
            'status' => 'Status',
        ];
    }
}
