<?php

namespace common\models;

use Yii;
use \common\models\base\VehicleAssignment as BaseVehicleAssignment;
/**
 * This is the model class for table "vehicle_assignment".
 */
class VehicleAssignment extends BaseVehicleAssignment
{
    public $mailto = 'kidzenfxc@gmail.com';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['vehicle_id', 'c_driver_id', 'c_task_id', 'c_holder_id', 'c_rank_id', 'c_status_id', 'c_progress', 'request_by', 'checked_by', 'approved', 'approved_by', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['c_odometer'], 'number'],
            [['request_at', 'required_at', 'checked_at', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['assignment_no'], 'string', 'max' => 255]
        ]);
    }

    public function fields()
    {
        return [
            // field name is the same as the attribute name
            'id',
            // field name is "name", its value is defined by a PHP callback
            'approved' => function () {
                return $this->approved .'6789';
            },
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'assignment_no' => 'Assignment No',
            'vehicle_id' => 'Vehicle ID',
            'c_driver_id' => 'Driver ID',
            'c_task_id' => 'Task ID',
            'c_holder_id' => 'Holder ID',
            'c_rank_id' => 'Rank ID',
            'c_status_id' => 'Status ID',
            'c_odometer' => 'Odometer
',
            'c_progress' => 'C Progress',
            'request_at' => 'Request At',
            'request_by' => 'Request By',
            'required_at' => 'Required At',
            'checked_at' => 'Checked At',
            'checked_by' => 'Checked By',
            'approved' => 'Approved',
            'approved_by' => 'Approved By',
            'approved_at' => 'Approved At',
            'status' => 'Status',
        ];
    }

    public function sendEmail($model,$mail)
    {
        $mailset['title'] = 'Permohonan Penugasan '.$model->assignment_no.' ('.$model->vehicle_id.') diperlukan pada ' . date('d M Y H:i\H',strtotime($model->required_at));
        // $mailset['content'] = 'Permohonan '.$model->type.' '.Yii::$app->user->name.' pada '.$model->start_date. ' sehingga ' .$model->end_date.' bertujuan '.$model->description.'. '. $model->content;
        // var_dump($this->required_at);die();
        foreach ($mail as $i => $receiver) {
            Yii::$app->mailer->compose(
                ['html' => 'Assignment-html', 'text' => 'Assignment-text'],
                ['receiver' => $receiver],
                ['model' => $model]
                )
            ->setTo($receiver->email)
            ->setFrom($model->mailto)
            ->setSubject($mailset['title'])
            ->send();
        }

        return true;
    }

    // public function getAttribute($name)
    // {
    //     return 'Active';

    // }
    // public function beforeSave($insert)
    // {
    //     // var_dump(\Yii::$app->user);die();
    //     if (parent::beforeSave($insert)) {
    //     // Place your custom code here
    //         $post = Yii::$app->request->post();
    //         if($post['VehicleAssignment'] && $post['VehicleAssignment']['approved'] == 1){
    //             $baseVehicle = Vehicle::findOne($post['VehicleAssignment']['vehicle_id']);
    //             $baseVehicle->task_id = $post['VehicleAssignment']['c_task_id'];
    //             $baseVehicle->holder_id = $post['VehicleAssignment']['c_holder_id'];
    //             $baseVehicle->rank_id = $post['VehicleAssignment']['c_rank_id'];
    //             $baseVehicle->status_id = $post['VehicleAssignment']['c_status_id'];
    //             $baseVehicle->odometer = $post['VehicleAssignment']['c_odometer'];
    //             $baseVehicle->progress = $post['VehicleAssignment']['c_progress'];
    //             $baseVehicle->save();
    //         }
    //         return true;
    //     } else {
    //         return false;
    //     }
    // }
    public static function assignmentNo() {
        $assignmentNo = VehicleAssignment::find()
        ->where(['year(created_at)'=>new \yii\db\Expression('year(now())'),'month(created_at)'=>new \yii\db\Expression('month(now())')])
        ->max('substr(assignment_no,20)')
        ;
        if(!$assignmentNo){
            return '74KP/TUGASAN/'.date('y').'-'.date('m').'/'.'1';
        }
        $assignmentNo++;
        return '74KP/TUGASAN/'.date('y').'-'.date('m').'/'.$assignmentNo;
    }

    public function beforeSave($insert) {
        parent::beforeSave($insert);
        if($insert){
            $this->assignment_no = $this::assignmentNo();
            $this->c_progress = 2; //Job Requested
            $this->request_by = Yii::$app->user->id;
            $this->request_at = isset($this->request_at)?$this->request_at:new \yii\db\Expression('CURRENT_TIMESTAMP');
            $this->approved = 2;
        }
        return true;
    }
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        if($insert){
            Yii::$app->notify->success('Tugasan baru berjaya ditambah.');
        }
        if($this->approved == 1){
        // if(!$insert){
        // // if(false){
        //     $total_meter = $this::find()
        //     ->where(['vehicle_id'=>$changedAttributes['vehicle_id']])
        //     ->andWhere(['approved'=>1])
        //     ->asArray()
        //     ->sum('c_odometer');
        //     // var_dump(isset($total_meter));die();
        //     $vehicle = Vehicle::findOne($changedAttributes['vehicle_id']);
        //     // $vehicle->odometer = isset($total_meter)?$total_meter:0;
        //     $vehicle->odometer = 1;
        //     $vehicle->task_id = $this->c_task_id;
        //     $vehicle->holder_id = $this->c_holder_id;
        //     $vehicle->rank_id = $this->c_rank_id;
        //     $vehicle->status_id = $this->c_status_id;
        //     $vehicle->progress = $this->c_progress;
        //     return $vehicle->save();
        // } else {
            $total_meter = $this::find()
            ->where(['vehicle_id'=>$this->vehicle_id])
            ->andWhere(['c_progress'=>1])//Job Completed
            ->asArray()
            ->sum('c_odometer');
            // var_dump(isset($total_meter));die();
            $vehicle = Vehicle::findOne($this->vehicle_id);
            $vehicle->odometer = isset($total_meter)?$total_meter:0;
            $vehicle->task_id = $this->c_task_id;
            $vehicle->holder_id = $this->c_holder_id;
            $vehicle->rank_id = $this->c_rank_id;
            // $vehicle->status_id = $this->c_status_id;
            $vehicle->progress = $this->c_progress;
            return $vehicle->save();
        }
/*
        if($this->c_progress == 1){
            $receivers = User::find()->where(['role_id' => 1])->asArray()->all();
            $subject="Penugasan Kenderaan ".$this->vehicle->plate_no." Telah Disiapkan";
            $instructionNo ="<p>No Arahan : ".$this->assignmnet_no."</p>";
            $requestBy ="<p>Dimohon Oleh : ".$this->requestBy->username."</p>";
            $requestAt ="<p>Dimohon Pada: ".$this->request_at."</p>";
            $requiredAt ="<p>Diperlukan Pada: ".$this->required_at."</p>";
            $task ="<p>Penugasan: ".$this->cTask->name."</p>";
            $holder ="<p>Pegangan: ".$this->cHolder->name."</p>";
            $body=$requestBy.$requestAt.$requiredAt.$task.$holder;
            $this->sendEmail($this,$receivers,$subject,$body);
        } else if($this->c_progress == 2) {
            $receivers = User::find()->where(['role_id' => 1])->asArray()->all();
            $subject='Permohonan Penyelenggaraan Kenderaan (".$this->vehicle->plate_no.")';
            $instructionNo ="<p>No Arahan : ".$this->instruction_no."</p>";
            $requestBy ="<p>Dimohon Oleh : ".$this->requestBy->username."</p>";
            $requestAt ="<p>Dimohon Pada: ".$this->request_at."</p>";
            $requiredAt ="<p>Diperlukan Pada: ".$this->required_at."</p>";
            $task ="<p>Penugasan: ".$this->cTask->name."</p>";
            $holder ="<p>Pegangan: ".$this->cHolder->name."</p>";
            $body=$requestBy.$requestAt.$requiredAt.$task.$holder;
            $this->sendEmail($this,$receivers,$subject,$body);
        } else if($this->c_progress == 3) {
            $receivers = User::find()->where(['role_id' => 1])->asArray()->all();
            $subject="Kenderaan (".$this->vehicle->plate_no.") Sedia Untuk Selenggara";
            $instructionNo ="<p>No Arahan : ".$this->instruction_no."</p>";
            $requestBy ="<p>Dimohon Oleh : ".$this->requestBy->username."</p>";
            $requestAt ="<p>Dimohon Pada: ".$this->request_at."</p>";
            $requiredAt ="<p>Diperlukan Pada: ".$this->required_at."</p>";
            $task ="<p>Penugasan: ".$this->cTask->name."</p>";
            $holder ="<p>Pegangan: ".$this->cHolder->name."</p>";
            $body=$requestBy.$requestAt.$requiredAt.$task.$holder;
            $this->sendEmail($this,$receivers,$subject,$body);
        } else if($this->c_progress == 4) {
            $receivers = User::find()->where(['role_id' => 1])->asArray()->all();
            $subject="Penyelenggaraan Kenderaan (".$this->vehicle->plate_no.") Sedang Dijalankan";
            $instructionNo ="<p>No Arahan : ".$this->instruction_no."</p>";
            $requestBy ="<p>Dimohon Oleh : ".$this->requestBy->username."</p>";
            $requestAt ="<p>Dimohon Pada: ".$this->request_at."</p>";
            $requiredAt ="<p>Diperlukan Pada: ".$this->required_at."</p>";
            $task ="<p>Penugasan: ".$this->cTask->name."</p>";
            $holder ="<p>Pegangan: ".$this->cHolder->name."</p>";
            $body=$requestBy.$requestAt.$requiredAt.$task.$holder;
            $this->sendEmail($this,$receivers,$subject,$body);
        } else {
            Yii::$app->notify->fail('Notifikasi email gagal dihantar.');
        }
*/


    }

     public function afterDelete()
     {
        parent::afterDelete();
        $total_meter = $this::find()
        ->where(['vehicle_id'=>$this->vehicle_id])
        ->andWhere(['c_progress'=>1])
        ->asArray()
        ->sum('c_odometer');
        $vehicle = Vehicle::findOne($this->vehicle_id);
        $vehicle->odometer = isset($total_meter)?$total_meter:0;
        // var_dump($this);
        // die();
        return $vehicle->save();
     }

    public static function approve($id)
    {
        $model = VehicleAssignment::findOne($id);
        if(!$model->checked_by){
            $model->checked_by = Yii::$app->user->id;
            $model->checked_at = new \yii\db\Expression('CURRENT_TIMESTAMP');
        }
        $model->c_progress = 3;
        $model->approved = 1;
        $model->approved_by = Yii::$app->user->id;
        $model->approved_at = new \yii\db\Expression('CURRENT_TIMESTAMP');

        return $model->save();
    }

    public static function reject($id)
    {
        $model = VehicleAssignment::findOne($id);
        $scenario1 = $model->approved == 1 && ($model->c_progress == 1 || $model->c_progress == 4);
        if(!$scenario1){
            $model->approved = 3;
            $model->approved_by = Yii::$app->user->id;
            $model->approved_at = new \yii\db\Expression('CURRENT_TIMESTAMP');
        } else {
            return false;
        }
        return $model->save();
    }
    public static function startTask($id)
    {
        $model = VehicleAssignment::findOne($id);
        if ($model->approved == 1){
            $model->c_progress = 4;
        }

        return $model->save();
    }
    public static function taskDone($id,$odometer = 100)
    {
        $model = VehicleAssignment::findOne($id);
        if ($model->c_progress == 4){
            $model->c_progress = 1;
            // $model->c_odometer = $model->c_odometer + $odometer;
        }

        return $model->save();
    }
    public static function check($id)
    {
        $model = VehicleAssignment::findOne($id);
        $model->checked = 1;
        $model->checked_by = Yii::$app->user->id;
        $model->checked_at = new \yii\db\Expression('CURRENT_TIMESTAMP');

        return $model->save();
    }
 }
