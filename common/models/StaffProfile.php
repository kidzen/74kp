<?php

namespace common\models;

use \common\models\base\StaffProfile as BaseStaffProfile;

/**
 * This is the model class for table "staff_profile".
 */
class StaffProfile extends BaseStaffProfile
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['user_id', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['name', 'nickname', 'staff_no'], 'required'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['nickname', 'staff_no'], 'string', 'max' => 20]
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'nickname' => 'Nickname',
            'staff_no' => 'Staff No',
            'status' => 'Status',
        ];
    }
}
