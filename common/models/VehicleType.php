<?php

namespace common\models;

use \common\models\base\VehicleType as BaseVehicleType;

/**
 * This is the model class for table "vehicle_type".
 */
class VehicleType extends BaseVehicleType
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['asset_type', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name', 'description', 'engine'], 'string', 'max' => 255]
        ]);
    }
	
    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'asset_type' => 'Asset Type',
            'name' => 'Name',
            'description' => 'Description',
            'engine' => 'Engine',
            'status' => 'Status',
        ];
    }
}
