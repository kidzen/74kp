<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "vehicle".
 *
 * @property integer $id
 * @property integer $type
 * @property string $model
 * @property string $plate_no
 * @property integer $task_id
 * @property integer $holder_id
 * @property integer $rank_id
 * @property integer $status_id
 * @property integer $breakdown_id
 * @property double $odometer
 * @property integer $progress
 * @property integer $status
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\VehicleType $type0
 * @property \common\models\VehicleTaskType $task
 * @property \common\models\VehicleHolderType $holder
 * @property \common\models\VehicleRankType $rank
 * @property \common\models\VehicleStatusType $status0
 * @property \common\models\VehicleBreakdownType $breakdown
 * @property \common\models\VehicleAssignment[] $vehicleAssignments
 * @property \common\models\VehicleCheckInstruction[] $vehicleCheckInstructions
 */
class Vehicle extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'task_id', 'holder_id', 'rank_id', 'status_id', 'breakdown_id', 'progress', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['plate_no'], 'required'],
            [['odometer'], 'number'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['model', 'plate_no'], 'string', 'max' => 255],
            [['plate_no'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vehicle';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'model' => 'Model',
            'plate_no' => 'Plate No',
            'task_id' => 'Task ID',
            'holder_id' => 'Holder ID',
            'rank_id' => 'Rank ID',
            'status_id' => 'Status ID',
            'breakdown_id' => 'Breakdown ID',
            'odometer' => 'Odometer',
            'progress' => 'Progress',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType0()
    {
        return $this->hasOne(\common\models\VehicleType::className(), ['id' => 'type'])->inverseOf('vehicles');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(\common\models\VehicleTaskType::className(), ['id' => 'task_id'])->inverseOf('vehicles');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHolder()
    {
        return $this->hasOne(\common\models\VehicleHolderType::className(), ['id' => 'holder_id'])->inverseOf('vehicles');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRank()
    {
        return $this->hasOne(\common\models\VehicleRankType::className(), ['id' => 'rank_id'])->inverseOf('vehicles');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(\common\models\VehicleStatusType::className(), ['id' => 'status_id'])->inverseOf('vehicles');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBreakdown()
    {
        return $this->hasOne(\common\models\VehicleBreakdownType::className(), ['id' => 'breakdown_id'])->inverseOf('vehicles');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicleAssignments()
    {
        return $this->hasMany(\common\models\VehicleAssignment::className(), ['vehicle_id' => 'id'])->inverseOf('vehicle');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicleCheckInstructions()
    {
        return $this->hasMany(\common\models\VehicleCheckInstruction::className(), ['vehicle_id' => 'id'])->inverseOf('vehicle');
    }

/**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\VehicleQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\VehicleQuery(get_called_class());
    }

    public function beforeSave($insert) {
        parent::beforeSave($insert);
        if($this->odometer >= 2000){
            $this->status = 3; //warning
            $this->status_id = 3; //warning
        }
        return true;
    }
}
