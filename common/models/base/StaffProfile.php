<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "staff_profile".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $name
 * @property string $nickname
 * @property string $staff_no
 * @property integer $status
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\Account[] $accounts
 * @property \common\models\Employment[] $employments
 * @property \common\models\Hod[] $hods
 * @property \common\models\LeaveRequest[] $leaveRequests
 * @property \common\models\User $user
 * @property \common\models\UserLeaveSetting[] $userLeaveSettings
 */
class StaffProfile extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['name', 'nickname', 'staff_no'], 'required'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['nickname', 'staff_no'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'staff_profile';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'name' => 'Name',
            'nickname' => 'Nickname',
            'staff_no' => 'Staff No',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccounts()
    {
        return $this->hasMany(\common\models\Account::className(), ['staff_id' => 'id'])->inverseOf('staff');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployments()
    {
        return $this->hasMany(\common\models\Employment::className(), ['approved_by' => 'id'])->inverseOf('staff')->inverseOf('approvedBy');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getHods()
    {
        return $this->hasMany(\common\models\Hod::className(), ['staff_id' => 'id'])->inverseOf('staff');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeaveRequests()
    {
        return $this->hasMany(\common\models\LeaveRequest::className(), ['approved_by' => 'id'])->inverseOf('staff')->inverseOf('approvedBy');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'user_id'])->inverseOf('staffProfiles');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLeaveSettings()
    {
        return $this->hasMany(\common\models\UserLeaveSetting::className(), ['staff_id' => 'id'])->inverseOf('staff');
    }

/**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\StaffProfileQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\StaffProfileQuery(get_called_class());
    }
}
