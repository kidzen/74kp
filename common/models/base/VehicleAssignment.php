<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
// use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "vehicle_assignment".
 *
 * @property integer $id
 * @property string $assignment_no
 * @property integer $vehicle_id
 * @property integer $c_driver_id
 * @property integer $c_task_id
 * @property integer $c_holder_id
 * @property integer $c_rank_id
 * @property integer $c_status_id
 * @property double $c_odometer
 * @property integer $c_progress
 * @property string $request_at
 * @property integer $request_by
 * @property string $required_at
 * @property string $checked_at
 * @property integer $checked_by
 * @property integer $approved
 * @property integer $approved_by
 * @property string $approved_at
 * @property integer $status
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\Vehicle $vehicle
 * @property \common\models\User $cDriver
 * @property \common\models\VehicleTaskType $cTask
 * @property \common\models\VehicleHolderType $cHolder
 * @property \common\models\VehicleRankType $cRank
 * @property \common\models\VehicleStatusType $cStatus
 * @property \common\models\User $requestBy
 * @property \common\models\User $approvedBy
 */
class VehicleAssignment extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['vehicle_id', 'c_driver_id', 'c_task_id', 'c_holder_id', 'c_rank_id', 'c_status_id', 'c_progress', 'request_by', 'checked_by', 'approved', 'approved_by', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['c_odometer'], 'number'],
            [['request_at', 'required_at', 'checked_at', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['assignment_no'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vehicle_assignment';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'assignment_no' => 'Assignment No',
            'vehicle_id' => 'Vehicle ID',
            'c_driver_id' => 'Driver ID',
            'c_task_id' => 'Task ID',
            'c_holder_id' => 'Holder ID',
            'c_rank_id' => 'Rank ID',
            'c_status_id' => 'Status ID',
            'c_odometer' => 'Odometer
',
            'c_progress' => 'C Progress',
            'request_at' => 'Request At',
            'request_by' => 'Request By',
            'required_at' => 'Required At',
            'checked_at' => 'Checked At',
            'checked_by' => 'Checked By',
            'approved' => 'Approved',
            'approved_by' => 'Approved By',
            'approved_at' => 'Approved At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicle()
    {
        return $this->hasOne(\common\models\Vehicle::className(), ['id' => 'vehicle_id'])->inverseOf('vehicleAssignments');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCDriver()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'c_driver_id'])->inverseOf('vehicleAssignments');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCTask()
    {
        return $this->hasOne(\common\models\VehicleTaskType::className(), ['id' => 'c_task_id'])->inverseOf('vehicleAssignments');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCHolder()
    {
        return $this->hasOne(\common\models\VehicleHolderType::className(), ['id' => 'c_holder_id'])->inverseOf('vehicleAssignments');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCRank()
    {
        return $this->hasOne(\common\models\VehicleRankType::className(), ['id' => 'c_rank_id'])->inverseOf('vehicleAssignments');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCStatus()
    {
        return $this->hasOne(\common\models\VehicleStatusType::className(), ['id' => 'c_status_id'])->inverseOf('vehicleAssignments');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'request_by'])->inverseOf('vehicleAssignments');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'approved_by'])->inverseOf('vehicleAssignments');
    }

    public function getCheckedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'checked_by'])->inverseOf('vehicleAssignments');
    }

/**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            // 'uuid' => [
            //     'class' => UUIDBehavior::className(),
            //     'column' => 'id',
            // ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\VehicleAssignmentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\VehicleAssignmentQuery(get_called_class());
    }
}
