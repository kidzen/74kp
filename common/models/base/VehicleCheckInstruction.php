<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
// use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "vehicle_check_instruction".
 *
 * @property integer $id
 * @property string $instruction_no
 * @property string $description
 * @property integer $department_id
 * @property integer $vehicle_id
 * @property integer $c_driver_id
 * @property integer $c_task_id
 * @property integer $c_holder_id
 * @property integer $c_rank_id
 * @property integer $c_status_id
 * @property integer $c_breakdown_id
 * @property double $c_odometer
 * @property integer $c_progress
 * @property string $request_at
 * @property integer $request_by
 * @property string $required_at
 * @property string $checked_at
 * @property integer $checked_by
 * @property integer $approved
 * @property integer $approved_by
 * @property string $approved_at
 * @property double $total_cost
 * @property integer $status
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\Vehicle $vehicle
 * @property \common\models\Department $department
 * @property \common\models\User $cDriver
 * @property \common\models\VehicleTaskType $cTask
 * @property \common\models\VehicleHolderType $cHolder
 * @property \common\models\VehicleRankType $cRank
 * @property \common\models\VehicleStatusType $cStatus
 * @property \common\models\VehicleBreakdownType $cBreakdown
 * @property \common\models\User $requestBy
 * @property \common\models\User $checkedBy
 * @property \common\models\User $approvedBy
 */
class VehicleCheckInstruction extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description', 'request_at', 'required_at', 'checked_at', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['department_id', 'vehicle_id', 'c_driver_id', 'c_task_id', 'c_holder_id', 'c_rank_id', 'c_status_id', 'c_breakdown_id', 'c_progress', 'request_by', 'checked_by', 'approved', 'approved_by', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['c_odometer', 'total_cost'], 'number'],
            [['instruction_no'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vehicle_check_instruction';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'instruction_no' => 'Instruction No',
            'description' => 'Description',
            'department_id' => 'Department ID',
            'vehicle_id' => 'Vehicle ID',
            'c_driver_id' => 'Driver ID',
            'c_task_id' => 'Task ID',
            'c_holder_id' => 'Holder ID',
            'c_rank_id' => 'Rank ID',
            'c_status_id' => 'Status ID',
            'c_breakdown_id' => 'Breakdown ID',
            'c_odometer' => 'Odometer',
            'c_progress' => 'C Progress',
            'request_at' => 'Request At',
            'request_by' => 'Request By',
            'required_at' => 'Required At',
            'checked_at' => 'Checked At',
            'checked_by' => 'Checked By',
            'approved' => 'Approved',
            'approved_by' => 'Approved By',
            'approved_at' => 'Approved At',
            'total_cost' => 'Total Cost',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicle()
    {
        return $this->hasOne(\common\models\Vehicle::className(), ['id' => 'vehicle_id'])->inverseOf('vehicleCheckInstructions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDepartment()
    {
        return $this->hasOne(\common\models\Department::className(), ['id' => 'department_id'])->inverseOf('vehicleCheckInstructions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCDriver()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'c_driver_id'])->inverseOf('vehicleCheckInstructions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCTask()
    {
        return $this->hasOne(\common\models\VehicleTaskType::className(), ['id' => 'c_task_id'])->inverseOf('vehicleCheckInstructions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCHolder()
    {
        return $this->hasOne(\common\models\VehicleHolderType::className(), ['id' => 'c_holder_id'])->inverseOf('vehicleCheckInstructions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCRank()
    {
        return $this->hasOne(\common\models\VehicleRankType::className(), ['id' => 'c_rank_id'])->inverseOf('vehicleCheckInstructions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCStatus()
    {
        return $this->hasOne(\common\models\VehicleStatusType::className(), ['id' => 'c_status_id'])->inverseOf('vehicleCheckInstructions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCBreakdown()
    {
        return $this->hasOne(\common\models\VehicleBreakdownType::className(), ['id' => 'c_breakdown_id'])->inverseOf('vehicleCheckInstructions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRequestBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'request_by'])->inverseOf('vehicleCheckInstructions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'checked_by'])->inverseOf('vehicleCheckInstructions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApprovedBy()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'approved_by'])->inverseOf('vehicleCheckInstructions');
    }

/**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            // 'uuid' => [
            //     'class' => UUIDBehavior::className(),
            //     'column' => 'id',
            // ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\VehicleCheckInstructionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\VehicleCheckInstructionQuery(get_called_class());
    }
}
