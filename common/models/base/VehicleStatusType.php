<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use mootensai\behaviors\UUIDBehavior;

/**
 * This is the base model class for table "vehicle_status_type".
 *
 * @property integer $id
 * @property string $code_status
 * @property string $description
 * @property integer $status
 * @property integer $deleted
 * @property string $deleted_at
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\Vehicle[] $vehicles
 * @property \common\models\VehicleAssignment[] $vehicleAssignments
 * @property \common\models\VehicleCheckInstruction[] $vehicleCheckInstructions
 */
class VehicleStatusType extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['code_status', 'description'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vehicle_status_type';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code_status' => 'Code Status',
            'description' => 'Description',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicles()
    {
        return $this->hasMany(\common\models\Vehicle::className(), ['status_id' => 'id'])->inverseOf('status0');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicleAssignments()
    {
        return $this->hasMany(\common\models\VehicleAssignment::className(), ['c_status_id' => 'id'])->inverseOf('cStatus');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicleCheckInstructions()
    {
        return $this->hasMany(\common\models\VehicleCheckInstruction::className(), ['c_status_id' => 'id'])->inverseOf('cStatus');
    }

/**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
            ],
            'uuid' => [
                'class' => UUIDBehavior::className(),
                'column' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return \common\models\query\VehicleStatusTypeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\VehicleStatusTypeQuery(get_called_class());
    }
}
