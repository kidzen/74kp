<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\VehicleCheckInstruction;

/**
 * common\models\search\VehicleCheckInstructionSearch represents the model behind the search form about `common\models\VehicleCheckInstruction`.
 */
 class VehicleCheckInstructionSearch extends VehicleCheckInstruction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'department_id', 'vehicle_id', 'c_driver_id', 'c_task_id', 'c_holder_id', 'c_rank_id', 'c_status_id', 'c_breakdown_id', 'c_progress', 'request_by', 'checked_by', 'approved', 'approved_by', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['instruction_no', 'description', 'request_at', 'required_at', 'checked_at', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['c_odometer', 'total_cost'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VehicleCheckInstruction::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'description' => $this->description,
            'department_id' => $this->department_id,
            'vehicle_id' => $this->vehicle_id,
            'c_driver_id' => $this->c_driver_id,
            'c_task_id' => $this->c_task_id,
            'c_holder_id' => $this->c_holder_id,
            'c_rank_id' => $this->c_rank_id,
            'c_status_id' => $this->c_status_id,
            'c_breakdown_id' => $this->c_breakdown_id,
            'c_odometer' => $this->c_odometer,
            'c_progress' => $this->c_progress,
            'request_at' => $this->request_at,
            'request_by' => $this->request_by,
            'required_at' => $this->required_at,
            'checked_at' => $this->checked_at,
            'checked_by' => $this->checked_by,
            'approved' => $this->approved,
            'approved_by' => $this->approved_by,
            'approved_at' => $this->approved_at,
            'total_cost' => $this->total_cost,
            'status' => $this->status,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'instruction_no', $this->instruction_no]);

        return $dataProvider;
    }
}
