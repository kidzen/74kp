<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Vehicle;

/**
 * common\models\search\VehicleSearch represents the model behind the search form about `common\models\Vehicle`.
 */
 class VehicleSearch extends Vehicle
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'type', 'task_id', 'holder_id', 'rank_id', 'status_id', 'breakdown_id', 'progress', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['model', 'plate_no', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['odometer'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Vehicle::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            'task_id' => $this->task_id,
            'holder_id' => $this->holder_id,
            'rank_id' => $this->rank_id,
            'status_id' => $this->status_id,
            'breakdown_id' => $this->breakdown_id,
            'odometer' => $this->odometer,
            'progress' => $this->progress,
            'status' => $this->status,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'plate_no', $this->plate_no]);

        return $dataProvider;
    }
}
