<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\PayrollBase;

/**
 * common\models\search\PayrollBaseSearch represents the model behind the search form about `common\models\PayrollBase`.
 */
 class PayrollBaseSearch extends PayrollBase
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'employment_id', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['valid_from', 'valid_to', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = PayrollBase::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'employment_id' => $this->employment_id,
            'valid_from' => $this->valid_from,
            'valid_to' => $this->valid_to,
            'status' => $this->status,
            'deleted' => $this->deleted,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        return $dataProvider;
    }
}
