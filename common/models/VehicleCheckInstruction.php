<?php

namespace common\models;

use \common\models\base\VehicleCheckInstruction as BaseVehicleCheckInstruction;
use common\components\Enum;
use Yii;
// use \common\models\VehicleCheckInstruction;

/**
 * This is the model class for table "vehicle_check_instruction".
 */
class VehicleCheckInstruction extends BaseVehicleCheckInstruction
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
        [
            // this part has validation problem
            // [['instruction_no'], 'required'],
            // [['vehicle_id'], 'required'],
            // [['vehicle_id','required_at','c_breakdown_id'], 'required'],
            [[ 'request_at', 'required_at', 'checked_at', 'approved_at', 'deleted_at', 'created_at', 'updated_at'], 'safe'],
            [['department_id', 'vehicle_id', 'c_driver_id', 'c_task_id', 'c_holder_id', 'c_rank_id', 'c_status_id', 'c_breakdown_id', 'c_progress', 'request_by', 'checked_by', 'approved', 'approved_by', 'status', 'deleted', 'created_by', 'updated_by'], 'integer'],
            [['c_odometer', 'total_cost'], 'number'],
            [['description', 'instruction_no'], 'string', 'max' => 255]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => 'ID',
            'instruction_no' => 'Instruction No',
            'description' => 'Description',
            'department_id' => 'Department ID',
            'vehicle_id' => 'Vehicle ID',
            'c_driver_id' => 'Driver ID',
            'c_task_id' => 'Task ID',
            'c_holder_id' => 'Holder ID',
            'c_rank_id' => 'Rank ID',
            'c_status_id' => 'Status ID',
            'c_breakdown_id' => 'Breakdown ID
',
            'c_odometer' => 'Odometer
',
            'c_progress' => 'C Progress',
            'request_at' => 'Request At',
            'request_by' => 'Request By',
            'required_at' => 'Required At',
            'checked_at' => 'Checked At',
            'checked_by' => 'Checked By',
            'approved' => 'Approved',
            'approved_by' => 'Approved By',
            'approved_at' => 'Approved At',
            'total_cost' => 'Total Cost',
            'status' => 'Status',
        ];
    }

    public function sendEmail($model,$receivers,$subject,$body)
    {
        // $content['title'] = 'Permohonan Penugasan '.$model->assignment_no.' ('.$model->vehicle_id.') diperlukan pada ' . date('d M Y H:i\H',strtotime($model->required_at));
        // $mailset['content'] = 'Permohonan '.$model->type.' '.Yii::$app->user->name.' pada '.$model->start_date. ' sehingga ' .$model->end_date.' bertujuan '.$model->description.'. '. $model->content;
        // var_dump($this->required_at);die();
/*
    <?php
        $req_date = new DateTime(Html::encode($model->required_at));
    ?>
    <p>Diperlukan pada : <?= Html::encode($req_date->format('d M Y H:i\H (l)')) ?></p>
*/
        $sender = \Yii::$app->params['adminEmail'];
        foreach ($receivers as $i => $receiver) {
            Yii::$app->mailer->compose(
                // ['html' => 'Assignment-html', 'text' => 'Assignment-text'],
                ['html' => 'Assignment-html'],
                [
                    'receiver' => $receiver,
                    'body' => $body,
                    'subject' => $subject,
                    'sender' => $sender,
                ]
                )
            ->setTo($receiver['email'])
            // ->setFrom($receiver['email'])
            ->setFrom($sender)
            ->setSubject($subject)
            ->send();
        }

        return true;
    }

    public static function instructionNo() {
        $instructionNo = VehicleCheckInstruction::find()
        ->where(['year(created_at)'=>new \yii\db\Expression('year(now())'),'month(created_at)'=>new \yii\db\Expression('month(now())')])
        ->max('substr(instruction_no,23)')
        ;
        if(!$instructionNo){
            return '74KP/SELENGGARA/'.date('y').'-'.date('m').'/'.'1';
            // return '74KP/TUGASAN/'.date('y').'-'.date('m').'/'.'1';
        }
        $instructionNo++;
        return '74KP/SELENGGARA/'.date('y').'-'.date('m').'/'.$instructionNo;
    }

    public function beforeSave($insert) {
        if($insert){
            $this->required_at = null;
            $this->instruction_no = $this::instructionNo();
            $this->c_progress = 6; //requested
            $this->vehicle->progress = $this->c_progress;
            $this->request_by = Yii::$app->user->id;
            $this->request_at = isset($this->request_at)?$this->request_at:new \yii\db\Expression('CURRENT_TIMESTAMP');
            $this->approved = 2; //pending
        }
        parent::beforeSave($insert);
        return true;
    }
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        if($insert){
            Yii::$app->notify->success('Permohonan selenggara berjaya dimohon.');
        }
        if($this->approved == 1){
            $this->vehicle->breakdown_id = $this->c_breakdown_id;
            $this->vehicle->progress = $this->c_progress;
            $this->vehicle->status_id = $this->c_status_id;
            return $this->vehicle->save();
        }

        if($this->c_progress == 5){
            $receivers = User::find()->where(['role_id' => 1])->asArray()->all();
            $subject="Penyelenggaraan Kenderaan ".$this->vehicle->plate_no." Telah Disiapkan";
            $instructionNo ="<p>No Arahan : ".$this->instruction_no."</p>";
            $requestBy ="<p>Dimohon Oleh : ".$this->requestBy->username."</p>";
            $requestAt ="<p>Dimohon Pada: ".$this->request_at."</p>";
            $requiredAt ="<p>Diperlukan Pada: ".$this->required_at."</p>";
            $breakdown ="<p>Jenis Kerosakan: ".$this->description."</p>";
            $description ="<p>Nota Kerosakan: ".$this->description."</p>";
            $body=$requestBy.$requestAt.$requestAt.$breakdown.$description;
            $this->sendEmail($this,$receivers,$subject,$body);
        } else if($this->c_progress == 6) {
            $receivers = User::find()->where(['role_id' => 1])->asArray()->all();
            $subject='Permohonan Penyelenggaraan Kenderaan (".$this->vehicle->plate_no.")';
            $instructionNo ="<p>No Arahan : ".$this->instruction_no."</p>";
            $requestBy ="<p>Dimohon Oleh : ".$this->requestBy->username."</p>";
            $requestAt ="<p>Dimohon Pada: ".$this->request_at."</p>";
            $requiredAt ="<p>Diperlukan Pada: ".$this->required_at."</p>";
            $breakdown ="<p>Jenis Kerosakan: ".$this->description."</p>";
            $description ="<p>Nota Kerosakan: ".$this->description."</p>";
            $body=$requestBy.$requestAt.$requestAt.$breakdown.$description;
            $this->sendEmail($this,$receivers,$subject,$body);
        } else if($this->c_progress == 7) {
            $receivers = User::find()->where(['role_id' => 1])->asArray()->all();
            $subject="Kenderaan (".$this->vehicle->plate_no.") Sedia Untuk Selenggara";
            $instructionNo ="<p>No Arahan : ".$this->instruction_no."</p>";
            $requestBy ="<p>Dimohon Oleh : ".$this->requestBy->username."</p>";
            $requestAt ="<p>Dimohon Pada: ".$this->request_at."</p>";
            $requiredAt ="<p>Diperlukan Pada: ".$this->required_at."</p>";
            $breakdown ="<p>Jenis Kerosakan: ".$this->description."</p>";
            $description ="<p>Nota Kerosakan: ".$this->description."</p>";
            $body=$requestBy.$requestAt.$requestAt.$breakdown.$description;
            $this->sendEmail($this,$receivers,$subject,$body);
        } else if($this->c_progress == 8) {
            $receivers = User::find()->where(['role_id' => 1])->asArray()->all();
            $subject="Penyelenggaraan Kenderaan (".$this->vehicle->plate_no.") Sedang Dijalankan";
            $instructionNo ="<p>No Arahan : ".$this->instruction_no."</p>";
            $requestBy ="<p>Dimohon Oleh : ".$this->requestBy->username."</p>";
            $requestAt ="<p>Dimohon Pada: ".$this->request_at."</p>";
            $requiredAt ="<p>Diperlukan Pada: ".$this->required_at."</p>";
            $breakdown ="<p>Jenis Kerosakan: ".$this->description."</p>";
            $description ="<p>Nota Kerosakan: ".$this->description."</p>";
            $body=$requestBy.$requestAt.$requestAt.$breakdown.$description;
            $this->sendEmail($this,$receivers,$subject,$body);
        } else {
            Yii::$app->notify->fail('Notifikasi email gagal dihantar.');
        }

        return true;
    }

     public function afterDelete()
     {
        parent::afterDelete();
        // $vehicle = Vehicle::findOne($this->vehicle_id);
        // $vehicle->odometer = isset($total_meter)?$total_meter:0;
        // var_dump($this);
        // die();
        // return $vehicle->save();
     }

    public static function approve($id)
    {
        // var_dump(Yii::$app->request->post());die();
        $model = VehicleCheckInstruction::findOne($id);
        if(!$model->checked_by){
            $model->checked_by = Yii::$app->user->id;
            $model->checked_at = new \yii\db\Expression('CURRENT_TIMESTAMP');
        }
        $model->c_progress = 7;
        $model->approved = 1;
        $model->approved_by = Yii::$app->user->id;
        $model->approved_at = new \yii\db\Expression('CURRENT_TIMESTAMP');

        return $model->save();
    }

    public static function reject($id)
    {
        $model = VehicleCheckInstruction::findOne($id);
        $scenario1 = $model->approved == 1 && ($model->c_progress == 1 || $model->c_progress == 4);
        if(!$scenario1){
            $model->approved = 3;
            $model->approved_by = Yii::$app->user->id;
            $model->approved_at = new \yii\db\Expression('CURRENT_TIMESTAMP');
        } else {
            return false;
        }
        return $model->save();
    }
    public static function startTask($id)
    {
        $model = VehicleCheckInstruction::findOne($id);
        if ($model->approved == 1){
            $model->c_progress = 8;
        }

        return $model->save();
    }
    public static function taskDone($id,$odometer = 100)
    {
        $model = VehicleCheckInstruction::findOne($id);
        if ($model->c_progress == 8){
            $model->c_progress = 5;
            // $model->c_odometer = $model->c_odometer + $odometer;
        }

        return $model->save();
    }
    public static function check($id)
    {
        $model = VehicleCheckInstruction::findOne($id);
        $model->checked = 1;
        $model->checked_by = Yii::$app->user->id;
        $model->checked_at = new \yii\db\Expression('CURRENT_TIMESTAMP');

        return $model->save();
    }
}
