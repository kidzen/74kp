<?php
return [
    'adminEmail' => 'admin@74KPD.gov.my',
    'supportEmail' => 'support@74KPD.gov.my',
    'user.passwordResetTokenExpire' => 3600,
];
