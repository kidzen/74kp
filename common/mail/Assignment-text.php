<?php

use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $user common\models\User */

?>

Hello <?= Html::encode($receiver['username']) ?>,
Daripada : <?= Html::encode($receiver['email']) ?>
Nama Pegawai : <?= Html::encode($receiver['email']) ?>
Subjek : Permohonan Pergerakan
	<?php
	$req_date = new DateTime(Html::encode($model->required_at));
	?>
Diperlukan pada : <?= Html::encode($req_date->format('d M Y H:i\H (l)')) ?>

